// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: prototype/builder_config.proto

package prototype

import (
	api "go.chromium.org/chromiumos/config/go/build/api"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Defines a builder from platform's point of view.
type BuilderConfig struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Whether this builder should be considered criticial.
	Critical bool `protobuf:"varint,1,opt,name=critical,proto3" json:"critical,omitempty"`
	// Defines the build target for this builder config.
	BuildTarget *api.SystemImage_BuildTarget `protobuf:"bytes,2,opt,name=build_target,json=buildTarget,proto3" json:"build_target,omitempty"`
}

func (x *BuilderConfig) Reset() {
	*x = BuilderConfig{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_builder_config_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BuilderConfig) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BuilderConfig) ProtoMessage() {}

func (x *BuilderConfig) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_builder_config_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BuilderConfig.ProtoReflect.Descriptor instead.
func (*BuilderConfig) Descriptor() ([]byte, []int) {
	return file_prototype_builder_config_proto_rawDescGZIP(), []int{0}
}

func (x *BuilderConfig) GetCritical() bool {
	if x != nil {
		return x.Critical
	}
	return false
}

func (x *BuilderConfig) GetBuildTarget() *api.SystemImage_BuildTarget {
	if x != nil {
		return x.BuildTarget
	}
	return nil
}

var File_prototype_builder_config_proto protoreflect.FileDescriptor

var file_prototype_builder_config_proto_rawDesc = []byte{
	0x0a, 0x1e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2f, 0x62, 0x75, 0x69, 0x6c,
	0x64, 0x65, 0x72, 0x5f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x09, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x1a, 0x27, 0x63, 0x68, 0x72,
	0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x2f, 0x61, 0x70,
	0x69, 0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x5f, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x7d, 0x0a, 0x0d, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x65, 0x72, 0x43,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x72, 0x69, 0x74, 0x69, 0x63, 0x61,
	0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x08, 0x63, 0x72, 0x69, 0x74, 0x69, 0x63, 0x61,
	0x6c, 0x12, 0x50, 0x0a, 0x0c, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x5f, 0x74, 0x61, 0x72, 0x67, 0x65,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2d, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69,
	0x75, 0x6d, 0x6f, 0x73, 0x2e, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x53,
	0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6d, 0x61, 0x67, 0x65, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64,
	0x54, 0x61, 0x72, 0x67, 0x65, 0x74, 0x52, 0x0b, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x54, 0x61, 0x72,
	0x67, 0x65, 0x74, 0x42, 0x35, 0x5a, 0x33, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69,
	0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f,
	0x73, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f,
	0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_prototype_builder_config_proto_rawDescOnce sync.Once
	file_prototype_builder_config_proto_rawDescData = file_prototype_builder_config_proto_rawDesc
)

func file_prototype_builder_config_proto_rawDescGZIP() []byte {
	file_prototype_builder_config_proto_rawDescOnce.Do(func() {
		file_prototype_builder_config_proto_rawDescData = protoimpl.X.CompressGZIP(file_prototype_builder_config_proto_rawDescData)
	})
	return file_prototype_builder_config_proto_rawDescData
}

var file_prototype_builder_config_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_prototype_builder_config_proto_goTypes = []interface{}{
	(*BuilderConfig)(nil),               // 0: prototype.BuilderConfig
	(*api.SystemImage_BuildTarget)(nil), // 1: chromiumos.build.api.SystemImage.BuildTarget
}
var file_prototype_builder_config_proto_depIdxs = []int32{
	1, // 0: prototype.BuilderConfig.build_target:type_name -> chromiumos.build.api.SystemImage.BuildTarget
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_prototype_builder_config_proto_init() }
func file_prototype_builder_config_proto_init() {
	if File_prototype_builder_config_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_prototype_builder_config_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BuilderConfig); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_prototype_builder_config_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_prototype_builder_config_proto_goTypes,
		DependencyIndexes: file_prototype_builder_config_proto_depIdxs,
		MessageInfos:      file_prototype_builder_config_proto_msgTypes,
	}.Build()
	File_prototype_builder_config_proto = out.File
	file_prototype_builder_config_proto_rawDesc = nil
	file_prototype_builder_config_proto_goTypes = nil
	file_prototype_builder_config_proto_depIdxs = nil
}
