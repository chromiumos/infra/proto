// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: prototype/client_profile.proto

package prototype

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	durationpb "google.golang.org/protobuf/types/known/durationpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Profile to coordinate behavioral configs of internal sub services e.g. test
// platform Next Tag: 4
type ClientProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name                string               `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Id                  *ClientProfileId     `protobuf:"bytes,2,opt,name=id,proto3" json:"id,omitempty"`
	TestPlatformProfile *TestPlatformProfile `protobuf:"bytes,3,opt,name=test_platform_profile,json=testPlatformProfile,proto3" json:"test_platform_profile,omitempty"`
}

func (x *ClientProfile) Reset() {
	*x = ClientProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ClientProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ClientProfile) ProtoMessage() {}

func (x *ClientProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ClientProfile.ProtoReflect.Descriptor instead.
func (*ClientProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{0}
}

func (x *ClientProfile) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ClientProfile) GetId() *ClientProfileId {
	if x != nil {
		return x.Id
	}
	return nil
}

func (x *ClientProfile) GetTestPlatformProfile() *TestPlatformProfile {
	if x != nil {
		return x.TestPlatformProfile
	}
	return nil
}

// TestPlatformProfile configures aspects of the test platform behaviour
// per-request. Configure the scheduling priorities in the test platform, retry
// behavior, timeout characteristics and alerting thresholds throughout the test
// platform stack. Next Tag: 5
type TestPlatformProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Scheduler  *SchedulerProfile  `protobuf:"bytes,1,opt,name=scheduler,proto3" json:"scheduler,omitempty"`
	Retry      *RetryProfile      `protobuf:"bytes,2,opt,name=retry,proto3" json:"retry,omitempty"`
	Timeout    *TimeoutProfile    `protobuf:"bytes,3,opt,name=timeout,proto3" json:"timeout,omitempty"`
	Monitoring *MonitoringProfile `protobuf:"bytes,4,opt,name=monitoring,proto3" json:"monitoring,omitempty"`
}

func (x *TestPlatformProfile) Reset() {
	*x = TestPlatformProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TestPlatformProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TestPlatformProfile) ProtoMessage() {}

func (x *TestPlatformProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TestPlatformProfile.ProtoReflect.Descriptor instead.
func (*TestPlatformProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{1}
}

func (x *TestPlatformProfile) GetScheduler() *SchedulerProfile {
	if x != nil {
		return x.Scheduler
	}
	return nil
}

func (x *TestPlatformProfile) GetRetry() *RetryProfile {
	if x != nil {
		return x.Retry
	}
	return nil
}

func (x *TestPlatformProfile) GetTimeout() *TimeoutProfile {
	if x != nil {
		return x.Timeout
	}
	return nil
}

func (x *TestPlatformProfile) GetMonitoring() *MonitoringProfile {
	if x != nil {
		return x.Monitoring
	}
	return nil
}

// SchedulerProfile controls the test platform scheduling behaviour.
// Next Tag: 5
type SchedulerProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The task scheduling pools this device may be used for.
	//
	// Historical note: The concept of managed pools (e.g.: DUT_POOL_CQ) was used
	// for automatic balancing of healthy devices in critical pools. Due to
	// merging of critical pools into a common scheduler pool, this auto-balancing
	// service is no longer required.
	//
	// Old enum values are now serialized to the JSON encoding of those enums
	// (e.g. "DUT_POOL_QUOTA").
	// See lab/device.proto:DeviceUnderTest::DutPool.
	Pools []string `protobuf:"bytes,1,rep,name=pools,proto3" json:"pools,omitempty"`
	// Types that are assignable to QosChannel:
	//	*SchedulerProfile_Priority
	//	*SchedulerProfile_QuotaAccount
	QosChannel isSchedulerProfile_QosChannel `protobuf_oneof:"qos_channel"`
}

func (x *SchedulerProfile) Reset() {
	*x = SchedulerProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SchedulerProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SchedulerProfile) ProtoMessage() {}

func (x *SchedulerProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SchedulerProfile.ProtoReflect.Descriptor instead.
func (*SchedulerProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{2}
}

func (x *SchedulerProfile) GetPools() []string {
	if x != nil {
		return x.Pools
	}
	return nil
}

func (m *SchedulerProfile) GetQosChannel() isSchedulerProfile_QosChannel {
	if m != nil {
		return m.QosChannel
	}
	return nil
}

func (x *SchedulerProfile) GetPriority() uint32 {
	if x, ok := x.GetQosChannel().(*SchedulerProfile_Priority); ok {
		return x.Priority
	}
	return 0
}

func (x *SchedulerProfile) GetQuotaAccount() string {
	if x, ok := x.GetQosChannel().(*SchedulerProfile_QuotaAccount); ok {
		return x.QuotaAccount
	}
	return ""
}

type isSchedulerProfile_QosChannel interface {
	isSchedulerProfile_QosChannel()
}

type SchedulerProfile_Priority struct {
	// Priority corresponding to a swarming task priority.
	// If specified, it should be in the range [50,255].
	// It will be used for any swarming tasks created by this run.
	//
	// Note that the scheduler behavior with a given priority depends on
	// other factors, such as pool. In particular, if requests are run in
	// a quotascheduler-controlled pool, then this priority will be ignored,
	// as priority will be determined by quota account balances.
	Priority uint32 `protobuf:"varint,2,opt,name=priority,proto3,oneof"`
}

type SchedulerProfile_QuotaAccount struct {
	// Quota account for both managed and unamanged pools.
	// It should be used if the request is scheduled on a pool
	// managed by QuotaScheduler. See go/qs-enabled-pools for details.
	// If set for requests on no QuotaScheduler pools,
	// it will be ignored and the request will default to running at
	// the lowest priority.
	QuotaAccount string `protobuf:"bytes,3,opt,name=quota_account,json=quotaAccount,proto3,oneof"`
}

func (*SchedulerProfile_Priority) isSchedulerProfile_QosChannel() {}

func (*SchedulerProfile_QuotaAccount) isSchedulerProfile_QosChannel() {}

// RetryPrfoile defines parameters that affect how failed tests within
// a request are retried.
type RetryProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Whether to allow test retries.
	Allow bool `protobuf:"varint,1,opt,name=allow,proto3" json:"allow,omitempty"`
	// Maximum number of retries of tests within this invocation to allow.
	// 0 = unlimited.
	Max uint32 `protobuf:"varint,2,opt,name=max,proto3" json:"max,omitempty"`
}

func (x *RetryProfile) Reset() {
	*x = RetryProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RetryProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RetryProfile) ProtoMessage() {}

func (x *RetryProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RetryProfile.ProtoReflect.Descriptor instead.
func (*RetryProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{3}
}

func (x *RetryProfile) GetAllow() bool {
	if x != nil {
		return x.Allow
	}
	return false
}

func (x *RetryProfile) GetMax() uint32 {
	if x != nil {
		return x.Max
	}
	return 0
}

// TimeoutProfile defines parameters related to timeouts.
type TimeoutProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Maximum duration for the entire request to be handled.
	MaximumDuration *durationpb.Duration `protobuf:"bytes,1,opt,name=maximum_duration,json=maximumDuration,proto3" json:"maximum_duration,omitempty"`
}

func (x *TimeoutProfile) Reset() {
	*x = TimeoutProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TimeoutProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TimeoutProfile) ProtoMessage() {}

func (x *TimeoutProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TimeoutProfile.ProtoReflect.Descriptor instead.
func (*TimeoutProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{4}
}

func (x *TimeoutProfile) GetMaximumDuration() *durationpb.Duration {
	if x != nil {
		return x.MaximumDuration
	}
	return nil
}

// MonitoringProfile controls the alerting and monitoring of requests
// associated with the profile throughout the test infrastructure
// stack.
type MonitoringProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TestPlatformAlertingThresholds *TestPlatformAlertingThresholds `protobuf:"bytes,1,opt,name=test_platform_alerting_thresholds,json=testPlatformAlertingThresholds,proto3" json:"test_platform_alerting_thresholds,omitempty"`
}

func (x *MonitoringProfile) Reset() {
	*x = MonitoringProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MonitoringProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MonitoringProfile) ProtoMessage() {}

func (x *MonitoringProfile) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MonitoringProfile.ProtoReflect.Descriptor instead.
func (*MonitoringProfile) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{5}
}

func (x *MonitoringProfile) GetTestPlatformAlertingThresholds() *TestPlatformAlertingThresholds {
	if x != nil {
		return x.TestPlatformAlertingThresholds
	}
	return nil
}

// TestPlatformAlertingThresholds capture alerting thresholds for test platform.
type TestPlatformAlertingThresholds struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *TestPlatformAlertingThresholds) Reset() {
	*x = TestPlatformAlertingThresholds{}
	if protoimpl.UnsafeEnabled {
		mi := &file_prototype_client_profile_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TestPlatformAlertingThresholds) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TestPlatformAlertingThresholds) ProtoMessage() {}

func (x *TestPlatformAlertingThresholds) ProtoReflect() protoreflect.Message {
	mi := &file_prototype_client_profile_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TestPlatformAlertingThresholds.ProtoReflect.Descriptor instead.
func (*TestPlatformAlertingThresholds) Descriptor() ([]byte, []int) {
	return file_prototype_client_profile_proto_rawDescGZIP(), []int{6}
}

var File_prototype_client_profile_proto protoreflect.FileDescriptor

var file_prototype_client_profile_proto_rawDesc = []byte{
	0x0a, 0x1e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2f, 0x63, 0x6c, 0x69, 0x65,
	0x6e, 0x74, 0x5f, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x09, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x1a, 0x21, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2f, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x70, 0x72,
	0x6f, 0x66, 0x69, 0x6c, 0x65, 0x5f, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f,
	0x64, 0x75, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa3,
	0x01, 0x0a, 0x0d, 0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65,
	0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x2a, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1a, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x43, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x52, 0x0a, 0x15, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x5f, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x54, 0x65, 0x73, 0x74,
	0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x52,
	0x13, 0x74, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50, 0x72, 0x6f,
	0x66, 0x69, 0x6c, 0x65, 0x22, 0xf2, 0x01, 0x0a, 0x13, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x39, 0x0a, 0x09,
	0x73, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x53, 0x63, 0x68, 0x65,
	0x64, 0x75, 0x6c, 0x65, 0x72, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x52, 0x09, 0x73, 0x63,
	0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x12, 0x2d, 0x0a, 0x05, 0x72, 0x65, 0x74, 0x72, 0x79,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79,
	0x70, 0x65, 0x2e, 0x52, 0x65, 0x74, 0x72, 0x79, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x52,
	0x05, 0x72, 0x65, 0x74, 0x72, 0x79, 0x12, 0x33, 0x0a, 0x07, 0x74, 0x69, 0x6d, 0x65, 0x6f, 0x75,
	0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x19, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74,
	0x79, 0x70, 0x65, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x6f, 0x75, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69,
	0x6c, 0x65, 0x52, 0x07, 0x74, 0x69, 0x6d, 0x65, 0x6f, 0x75, 0x74, 0x12, 0x3c, 0x0a, 0x0a, 0x6d,
	0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x4d, 0x6f, 0x6e, 0x69,
	0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x52, 0x0a, 0x6d,
	0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x22, 0x7c, 0x0a, 0x10, 0x53, 0x63, 0x68,
	0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x70, 0x6f, 0x6f, 0x6c, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x05, 0x70, 0x6f,
	0x6f, 0x6c, 0x73, 0x12, 0x1c, 0x0a, 0x08, 0x70, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0d, 0x48, 0x00, 0x52, 0x08, 0x70, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74,
	0x79, 0x12, 0x25, 0x0a, 0x0d, 0x71, 0x75, 0x6f, 0x74, 0x61, 0x5f, 0x61, 0x63, 0x63, 0x6f, 0x75,
	0x6e, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x48, 0x00, 0x52, 0x0c, 0x71, 0x75, 0x6f, 0x74,
	0x61, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x42, 0x0d, 0x0a, 0x0b, 0x71, 0x6f, 0x73, 0x5f,
	0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x22, 0x36, 0x0a, 0x0c, 0x52, 0x65, 0x74, 0x72, 0x79,
	0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x61, 0x6c, 0x6c, 0x6f, 0x77,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05, 0x61, 0x6c, 0x6c, 0x6f, 0x77, 0x12, 0x10, 0x0a,
	0x03, 0x6d, 0x61, 0x78, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x6d, 0x61, 0x78, 0x22,
	0x56, 0x0a, 0x0e, 0x54, 0x69, 0x6d, 0x65, 0x6f, 0x75, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c,
	0x65, 0x12, 0x44, 0x0a, 0x10, 0x6d, 0x61, 0x78, 0x69, 0x6d, 0x75, 0x6d, 0x5f, 0x64, 0x75, 0x72,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x19, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x44, 0x75,
	0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x0f, 0x6d, 0x61, 0x78, 0x69, 0x6d, 0x75, 0x6d, 0x44,
	0x75, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x89, 0x01, 0x0a, 0x11, 0x4d, 0x6f, 0x6e, 0x69,
	0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x74, 0x0a,
	0x21, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x5f, 0x61,
	0x6c, 0x65, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x5f, 0x74, 0x68, 0x72, 0x65, 0x73, 0x68, 0x6f, 0x6c,
	0x64, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x74, 0x79, 0x70, 0x65, 0x2e, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x41, 0x6c, 0x65, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x54, 0x68, 0x72, 0x65, 0x73, 0x68, 0x6f,
	0x6c, 0x64, 0x73, 0x52, 0x1e, 0x74, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x41, 0x6c, 0x65, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x54, 0x68, 0x72, 0x65, 0x73, 0x68, 0x6f,
	0x6c, 0x64, 0x73, 0x22, 0x20, 0x0a, 0x1e, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x41, 0x6c, 0x65, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x54, 0x68, 0x72, 0x65, 0x73,
	0x68, 0x6f, 0x6c, 0x64, 0x73, 0x42, 0x35, 0x5a, 0x33, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f,
	0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75,
	0x6d, 0x6f, 0x73, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f,
	0x67, 0x6f, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x74, 0x79, 0x70, 0x65, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_prototype_client_profile_proto_rawDescOnce sync.Once
	file_prototype_client_profile_proto_rawDescData = file_prototype_client_profile_proto_rawDesc
)

func file_prototype_client_profile_proto_rawDescGZIP() []byte {
	file_prototype_client_profile_proto_rawDescOnce.Do(func() {
		file_prototype_client_profile_proto_rawDescData = protoimpl.X.CompressGZIP(file_prototype_client_profile_proto_rawDescData)
	})
	return file_prototype_client_profile_proto_rawDescData
}

var file_prototype_client_profile_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_prototype_client_profile_proto_goTypes = []interface{}{
	(*ClientProfile)(nil),                  // 0: prototype.ClientProfile
	(*TestPlatformProfile)(nil),            // 1: prototype.TestPlatformProfile
	(*SchedulerProfile)(nil),               // 2: prototype.SchedulerProfile
	(*RetryProfile)(nil),                   // 3: prototype.RetryProfile
	(*TimeoutProfile)(nil),                 // 4: prototype.TimeoutProfile
	(*MonitoringProfile)(nil),              // 5: prototype.MonitoringProfile
	(*TestPlatformAlertingThresholds)(nil), // 6: prototype.TestPlatformAlertingThresholds
	(*ClientProfileId)(nil),                // 7: prototype.ClientProfileId
	(*durationpb.Duration)(nil),            // 8: google.protobuf.Duration
}
var file_prototype_client_profile_proto_depIdxs = []int32{
	7, // 0: prototype.ClientProfile.id:type_name -> prototype.ClientProfileId
	1, // 1: prototype.ClientProfile.test_platform_profile:type_name -> prototype.TestPlatformProfile
	2, // 2: prototype.TestPlatformProfile.scheduler:type_name -> prototype.SchedulerProfile
	3, // 3: prototype.TestPlatformProfile.retry:type_name -> prototype.RetryProfile
	4, // 4: prototype.TestPlatformProfile.timeout:type_name -> prototype.TimeoutProfile
	5, // 5: prototype.TestPlatformProfile.monitoring:type_name -> prototype.MonitoringProfile
	8, // 6: prototype.TimeoutProfile.maximum_duration:type_name -> google.protobuf.Duration
	6, // 7: prototype.MonitoringProfile.test_platform_alerting_thresholds:type_name -> prototype.TestPlatformAlertingThresholds
	8, // [8:8] is the sub-list for method output_type
	8, // [8:8] is the sub-list for method input_type
	8, // [8:8] is the sub-list for extension type_name
	8, // [8:8] is the sub-list for extension extendee
	0, // [0:8] is the sub-list for field type_name
}

func init() { file_prototype_client_profile_proto_init() }
func file_prototype_client_profile_proto_init() {
	if File_prototype_client_profile_proto != nil {
		return
	}
	file_prototype_client_profile_id_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_prototype_client_profile_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ClientProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TestPlatformProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SchedulerProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RetryProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TimeoutProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MonitoringProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_prototype_client_profile_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TestPlatformAlertingThresholds); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_prototype_client_profile_proto_msgTypes[2].OneofWrappers = []interface{}{
		(*SchedulerProfile_Priority)(nil),
		(*SchedulerProfile_QuotaAccount)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_prototype_client_profile_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_prototype_client_profile_proto_goTypes,
		DependencyIndexes: file_prototype_client_profile_proto_depIdxs,
		MessageInfos:      file_prototype_client_profile_proto_msgTypes,
	}.Build()
	File_prototype_client_profile_proto = out.File
	file_prototype_client_profile_proto_rawDesc = nil
	file_prototype_client_profile_proto_goTypes = nil
	file_prototype_client_profile_proto_depIdxs = nil
}
