// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: manufacturing/config_id.proto

package manufacturing

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ConfigID struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The ID that uniquely identifies the hardware configuration of a ChromeOS
	// device.
	Value string `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
}

func (x *ConfigID) Reset() {
	*x = ConfigID{}
	if protoimpl.UnsafeEnabled {
		mi := &file_manufacturing_config_id_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConfigID) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConfigID) ProtoMessage() {}

func (x *ConfigID) ProtoReflect() protoreflect.Message {
	mi := &file_manufacturing_config_id_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConfigID.ProtoReflect.Descriptor instead.
func (*ConfigID) Descriptor() ([]byte, []int) {
	return file_manufacturing_config_id_proto_rawDescGZIP(), []int{0}
}

func (x *ConfigID) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

var File_manufacturing_config_id_proto protoreflect.FileDescriptor

var file_manufacturing_config_id_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x6d, 0x61, 0x6e, 0x75, 0x66, 0x61, 0x63, 0x74, 0x75, 0x72, 0x69, 0x6e, 0x67, 0x2f,
	0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x0d, 0x6d, 0x61, 0x6e, 0x75, 0x66, 0x61, 0x63, 0x74, 0x75, 0x72, 0x69, 0x6e, 0x67, 0x22, 0x20,
	0x0a, 0x08, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x49, 0x44, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x42, 0x39, 0x5a, 0x37, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e,
	0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69,
	0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x6d, 0x61,
	0x6e, 0x75, 0x66, 0x61, 0x63, 0x74, 0x75, 0x72, 0x69, 0x6e, 0x67, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_manufacturing_config_id_proto_rawDescOnce sync.Once
	file_manufacturing_config_id_proto_rawDescData = file_manufacturing_config_id_proto_rawDesc
)

func file_manufacturing_config_id_proto_rawDescGZIP() []byte {
	file_manufacturing_config_id_proto_rawDescOnce.Do(func() {
		file_manufacturing_config_id_proto_rawDescData = protoimpl.X.CompressGZIP(file_manufacturing_config_id_proto_rawDescData)
	})
	return file_manufacturing_config_id_proto_rawDescData
}

var file_manufacturing_config_id_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_manufacturing_config_id_proto_goTypes = []interface{}{
	(*ConfigID)(nil), // 0: manufacturing.ConfigID
}
var file_manufacturing_config_id_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_manufacturing_config_id_proto_init() }
func file_manufacturing_config_id_proto_init() {
	if File_manufacturing_config_id_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_manufacturing_config_id_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConfigID); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_manufacturing_config_id_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_manufacturing_config_id_proto_goTypes,
		DependencyIndexes: file_manufacturing_config_id_proto_depIdxs,
		MessageInfos:      file_manufacturing_config_id_proto_msgTypes,
	}.Build()
	File_manufacturing_config_id_proto = out.File
	file_manufacturing_config_id_proto_rawDesc = nil
	file_manufacturing_config_id_proto_goTypes = nil
	file_manufacturing_config_id_proto_depIdxs = nil
}
