// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: test_platform/skylab_test_runner/steps/test_execution.proto

package steps

import (
	api "go.chromium.org/chromiumos/config/go/test/api"
	skylab_test_runner "go.chromium.org/chromiumos/infra/proto/go/test_platform/skylab_test_runner"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// RunTestsRequest is the input for the Go-based replacement of test_runner.
type RunTestsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Request                      *skylab_test_runner.Request               `protobuf:"bytes,1,opt,name=request,proto3" json:"request,omitempty"`
	Config                       *skylab_test_runner.Config                `protobuf:"bytes,2,opt,name=config,proto3" json:"config,omitempty"`
	CftTestRequest               *skylab_test_runner.CFTTestRequest        `protobuf:"bytes,3,opt,name=cft_test_request,json=cftTestRequest,proto3" json:"cft_test_request,omitempty"`
	CommonConfig                 *skylab_test_runner.CommonConfig          `protobuf:"bytes,4,opt,name=common_config,json=commonConfig,proto3" json:"common_config,omitempty"`
	CrosTestRunnerRequest        *skylab_test_runner.CrosTestRunnerRequest `protobuf:"bytes,5,opt,name=cros_test_runner_request,json=crosTestRunnerRequest,proto3" json:"cros_test_runner_request,omitempty"`
	CrosTestRunnerDynamicRequest *api.CrosTestRunnerDynamicRequest         `protobuf:"bytes,6,opt,name=cros_test_runner_dynamic_request,json=crosTestRunnerDynamicRequest,proto3" json:"cros_test_runner_dynamic_request,omitempty"`
	IsAlRun                      bool                                      `protobuf:"varint,7,opt,name=is_al_run,json=isAlRun,proto3" json:"is_al_run,omitempty"`
}

func (x *RunTestsRequest) Reset() {
	*x = RunTestsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RunTestsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RunTestsRequest) ProtoMessage() {}

func (x *RunTestsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RunTestsRequest.ProtoReflect.Descriptor instead.
func (*RunTestsRequest) Descriptor() ([]byte, []int) {
	return file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescGZIP(), []int{0}
}

func (x *RunTestsRequest) GetRequest() *skylab_test_runner.Request {
	if x != nil {
		return x.Request
	}
	return nil
}

func (x *RunTestsRequest) GetConfig() *skylab_test_runner.Config {
	if x != nil {
		return x.Config
	}
	return nil
}

func (x *RunTestsRequest) GetCftTestRequest() *skylab_test_runner.CFTTestRequest {
	if x != nil {
		return x.CftTestRequest
	}
	return nil
}

func (x *RunTestsRequest) GetCommonConfig() *skylab_test_runner.CommonConfig {
	if x != nil {
		return x.CommonConfig
	}
	return nil
}

func (x *RunTestsRequest) GetCrosTestRunnerRequest() *skylab_test_runner.CrosTestRunnerRequest {
	if x != nil {
		return x.CrosTestRunnerRequest
	}
	return nil
}

func (x *RunTestsRequest) GetCrosTestRunnerDynamicRequest() *api.CrosTestRunnerDynamicRequest {
	if x != nil {
		return x.CrosTestRunnerDynamicRequest
	}
	return nil
}

func (x *RunTestsRequest) GetIsAlRun() bool {
	if x != nil {
		return x.IsAlRun
	}
	return false
}

// RunTestsResponse is the output from the Go-based replacement of test_runner.
type RunTestsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// A markdown string that should be used in Buildbucket as a summary of the
	// test_runner build.
	ErrorSummaryMarkdown string `protobuf:"bytes,1,opt,name=error_summary_markdown,json=errorSummaryMarkdown,proto3" json:"error_summary_markdown,omitempty"`
	// compressed result that will be read by CTP.
	CompressedResult string `protobuf:"bytes,2,opt,name=compressed_result,proto3" json:"compressed_result,omitempty"`
}

func (x *RunTestsResponse) Reset() {
	*x = RunTestsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RunTestsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RunTestsResponse) ProtoMessage() {}

func (x *RunTestsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RunTestsResponse.ProtoReflect.Descriptor instead.
func (*RunTestsResponse) Descriptor() ([]byte, []int) {
	return file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescGZIP(), []int{1}
}

func (x *RunTestsResponse) GetErrorSummaryMarkdown() string {
	if x != nil {
		return x.ErrorSummaryMarkdown
	}
	return ""
}

func (x *RunTestsResponse) GetCompressedResult() string {
	if x != nil {
		return x.CompressedResult
	}
	return ""
}

var File_test_platform_skylab_test_runner_steps_test_execution_proto protoreflect.FileDescriptor

var file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDesc = []byte{
	0x0a, 0x3b, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f,
	0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e,
	0x65, 0x72, 0x2f, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x65, 0x78,
	0x65, 0x63, 0x75, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x26, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x6b, 0x79,
	0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e,
	0x73, 0x74, 0x65, 0x70, 0x73, 0x1a, 0x2d, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74,
	0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f,
	0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x32, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f,
	0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2f, 0x63, 0x66, 0x74, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x34, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70,
	0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x5f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x3f,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x6b,
	0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72,
	0x2f, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65,
	0x72, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a,
	0x26, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x74, 0x65, 0x73, 0x74,
	0x2f, 0x61, 0x70, 0x69, 0x2f, 0x74, 0x72, 0x76, 0x32, 0x5f, 0x64, 0x79, 0x6e, 0x61, 0x6d, 0x69,
	0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xd2, 0x04, 0x0a, 0x0f, 0x52, 0x75, 0x6e, 0x54,
	0x65, 0x73, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x43, 0x0a, 0x07, 0x72,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x6b, 0x79,
	0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x07, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x40, 0x0a, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x28, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x2e, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e,
	0x6e, 0x65, 0x72, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x06, 0x63, 0x6f, 0x6e, 0x66,
	0x69, 0x67, 0x12, 0x5a, 0x0a, 0x10, 0x63, 0x66, 0x74, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x30, 0x2e, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x6b, 0x79,
	0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e,
	0x43, 0x46, 0x54, 0x54, 0x65, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x0e,
	0x63, 0x66, 0x74, 0x54, 0x65, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x53,
	0x0a, 0x0d, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x5f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2e, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x43,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x0c, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x43, 0x6f, 0x6e,
	0x66, 0x69, 0x67, 0x12, 0x70, 0x0a, 0x18, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74,
	0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x37, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e, 0x43, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73,
	0x74, 0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x15,
	0x63, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x79, 0x0a, 0x20, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65,
	0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x5f, 0x64, 0x79, 0x6e, 0x61, 0x6d, 0x69,
	0x63, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x31, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2e, 0x74, 0x65, 0x73,
	0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x43, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x52, 0x75,
	0x6e, 0x6e, 0x65, 0x72, 0x44, 0x79, 0x6e, 0x61, 0x6d, 0x69, 0x63, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x52, 0x1c, 0x63, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x52, 0x75, 0x6e, 0x6e,
	0x65, 0x72, 0x44, 0x79, 0x6e, 0x61, 0x6d, 0x69, 0x63, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x1a, 0x0a, 0x09, 0x69, 0x73, 0x5f, 0x61, 0x6c, 0x5f, 0x72, 0x75, 0x6e, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x08, 0x52, 0x07, 0x69, 0x73, 0x41, 0x6c, 0x52, 0x75, 0x6e, 0x22, 0x76, 0x0a, 0x10,
	0x52, 0x75, 0x6e, 0x54, 0x65, 0x73, 0x74, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x34, 0x0a, 0x16, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x5f, 0x73, 0x75, 0x6d, 0x6d, 0x61, 0x72,
	0x79, 0x5f, 0x6d, 0x61, 0x72, 0x6b, 0x64, 0x6f, 0x77, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x14, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x53, 0x75, 0x6d, 0x6d, 0x61, 0x72, 0x79, 0x4d, 0x61,
	0x72, 0x6b, 0x64, 0x6f, 0x77, 0x6e, 0x12, 0x2c, 0x0a, 0x11, 0x63, 0x6f, 0x6d, 0x70, 0x72, 0x65,
	0x73, 0x73, 0x65, 0x64, 0x5f, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x11, 0x63, 0x6f, 0x6d, 0x70, 0x72, 0x65, 0x73, 0x73, 0x65, 0x64, 0x5f, 0x72, 0x65,
	0x73, 0x75, 0x6c, 0x74, 0x42, 0x52, 0x5a, 0x50, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d,
	0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d,
	0x6f, 0x73, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67,
	0x6f, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f,
	0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e,
	0x65, 0x72, 0x2f, 0x73, 0x74, 0x65, 0x70, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescOnce sync.Once
	file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescData = file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDesc
)

func file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescGZIP() []byte {
	file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescOnce.Do(func() {
		file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescData = protoimpl.X.CompressGZIP(file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescData)
	})
	return file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDescData
}

var file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_test_platform_skylab_test_runner_steps_test_execution_proto_goTypes = []interface{}{
	(*RunTestsRequest)(nil),                          // 0: test_platform.skylab_test_runner.steps.RunTestsRequest
	(*RunTestsResponse)(nil),                         // 1: test_platform.skylab_test_runner.steps.RunTestsResponse
	(*skylab_test_runner.Request)(nil),               // 2: test_platform.skylab_test_runner.Request
	(*skylab_test_runner.Config)(nil),                // 3: test_platform.skylab_test_runner.Config
	(*skylab_test_runner.CFTTestRequest)(nil),        // 4: test_platform.skylab_test_runner.CFTTestRequest
	(*skylab_test_runner.CommonConfig)(nil),          // 5: test_platform.skylab_test_runner.CommonConfig
	(*skylab_test_runner.CrosTestRunnerRequest)(nil), // 6: test_platform.skylab_test_runner.CrosTestRunnerRequest
	(*api.CrosTestRunnerDynamicRequest)(nil),         // 7: chromiumos.test.api.CrosTestRunnerDynamicRequest
}
var file_test_platform_skylab_test_runner_steps_test_execution_proto_depIdxs = []int32{
	2, // 0: test_platform.skylab_test_runner.steps.RunTestsRequest.request:type_name -> test_platform.skylab_test_runner.Request
	3, // 1: test_platform.skylab_test_runner.steps.RunTestsRequest.config:type_name -> test_platform.skylab_test_runner.Config
	4, // 2: test_platform.skylab_test_runner.steps.RunTestsRequest.cft_test_request:type_name -> test_platform.skylab_test_runner.CFTTestRequest
	5, // 3: test_platform.skylab_test_runner.steps.RunTestsRequest.common_config:type_name -> test_platform.skylab_test_runner.CommonConfig
	6, // 4: test_platform.skylab_test_runner.steps.RunTestsRequest.cros_test_runner_request:type_name -> test_platform.skylab_test_runner.CrosTestRunnerRequest
	7, // 5: test_platform.skylab_test_runner.steps.RunTestsRequest.cros_test_runner_dynamic_request:type_name -> chromiumos.test.api.CrosTestRunnerDynamicRequest
	6, // [6:6] is the sub-list for method output_type
	6, // [6:6] is the sub-list for method input_type
	6, // [6:6] is the sub-list for extension type_name
	6, // [6:6] is the sub-list for extension extendee
	0, // [0:6] is the sub-list for field type_name
}

func init() { file_test_platform_skylab_test_runner_steps_test_execution_proto_init() }
func file_test_platform_skylab_test_runner_steps_test_execution_proto_init() {
	if File_test_platform_skylab_test_runner_steps_test_execution_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RunTestsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RunTestsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_test_platform_skylab_test_runner_steps_test_execution_proto_goTypes,
		DependencyIndexes: file_test_platform_skylab_test_runner_steps_test_execution_proto_depIdxs,
		MessageInfos:      file_test_platform_skylab_test_runner_steps_test_execution_proto_msgTypes,
	}.Build()
	File_test_platform_skylab_test_runner_steps_test_execution_proto = out.File
	file_test_platform_skylab_test_runner_steps_test_execution_proto_rawDesc = nil
	file_test_platform_skylab_test_runner_steps_test_execution_proto_goTypes = nil
	file_test_platform_skylab_test_runner_steps_test_execution_proto_depIdxs = nil
}
