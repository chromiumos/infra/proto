// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: test_platform/steps/enumeration.proto

package steps

import (
	api "go.chromium.org/chromiumos/infra/proto/go/chromite/api"
	test_platform "go.chromium.org/chromiumos/infra/proto/go/test_platform"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type EnumerationRequests struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Each EnumerationRequest is handled independently.
	Requests       []*EnumerationRequest          `protobuf:"bytes,1,rep,name=requests,proto3" json:"requests,omitempty"`
	TaggedRequests map[string]*EnumerationRequest `protobuf:"bytes,2,rep,name=tagged_requests,json=taggedRequests,proto3" json:"tagged_requests,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (x *EnumerationRequests) Reset() {
	*x = EnumerationRequests{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_steps_enumeration_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EnumerationRequests) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EnumerationRequests) ProtoMessage() {}

func (x *EnumerationRequests) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_steps_enumeration_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EnumerationRequests.ProtoReflect.Descriptor instead.
func (*EnumerationRequests) Descriptor() ([]byte, []int) {
	return file_test_platform_steps_enumeration_proto_rawDescGZIP(), []int{0}
}

func (x *EnumerationRequests) GetRequests() []*EnumerationRequest {
	if x != nil {
		return x.Requests
	}
	return nil
}

func (x *EnumerationRequests) GetTaggedRequests() map[string]*EnumerationRequest {
	if x != nil {
		return x.TaggedRequests
	}
	return nil
}

type EnumerationResponses struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Each EnumerationResponse corresponds to a single EnumerationRequest
	// handled independently.
	Responses       []*EnumerationResponse          `protobuf:"bytes,1,rep,name=responses,proto3" json:"responses,omitempty"`
	TaggedResponses map[string]*EnumerationResponse `protobuf:"bytes,2,rep,name=tagged_responses,json=taggedResponses,proto3" json:"tagged_responses,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (x *EnumerationResponses) Reset() {
	*x = EnumerationResponses{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_steps_enumeration_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EnumerationResponses) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EnumerationResponses) ProtoMessage() {}

func (x *EnumerationResponses) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_steps_enumeration_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EnumerationResponses.ProtoReflect.Descriptor instead.
func (*EnumerationResponses) Descriptor() ([]byte, []int) {
	return file_test_platform_steps_enumeration_proto_rawDescGZIP(), []int{1}
}

func (x *EnumerationResponses) GetResponses() []*EnumerationResponse {
	if x != nil {
		return x.Responses
	}
	return nil
}

func (x *EnumerationResponses) GetTaggedResponses() map[string]*EnumerationResponse {
	if x != nil {
		return x.TaggedResponses
	}
	return nil
}

// EnumerationRequest defines the input of the test enumeration step.
type EnumerationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Metadata *test_platform.Request_Params_Metadata `protobuf:"bytes,1,opt,name=metadata,proto3" json:"metadata,omitempty"`
	TestPlan *test_platform.Request_TestPlan        `protobuf:"bytes,4,opt,name=test_plan,json=testPlan,proto3" json:"test_plan,omitempty"`
}

func (x *EnumerationRequest) Reset() {
	*x = EnumerationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_steps_enumeration_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EnumerationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EnumerationRequest) ProtoMessage() {}

func (x *EnumerationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_steps_enumeration_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EnumerationRequest.ProtoReflect.Descriptor instead.
func (*EnumerationRequest) Descriptor() ([]byte, []int) {
	return file_test_platform_steps_enumeration_proto_rawDescGZIP(), []int{2}
}

func (x *EnumerationRequest) GetMetadata() *test_platform.Request_Params_Metadata {
	if x != nil {
		return x.Metadata
	}
	return nil
}

func (x *EnumerationRequest) GetTestPlan() *test_platform.Request_TestPlan {
	if x != nil {
		return x.TestPlan
	}
	return nil
}

// EnumerationResponse defines the output of the test enumeration step.
//
// This is copied in test_platform.Request.Enumeration
// Keep in sync.
type EnumerationResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AutotestInvocations []*EnumerationResponse_AutotestInvocation `protobuf:"bytes,2,rep,name=autotest_invocations,json=autotestInvocations,proto3" json:"autotest_invocations,omitempty"`
	// If set, a summary of the errors encountered during enumeration.
	//
	// If set, the corresponding request's final verdict must reflect failure.
	//
	// autotest_invocations may be non-empty even when error_summary is set.
	// In that case all included autotest_invocations must be executed, despite
	// the error_summary.
	ErrorSummary string `protobuf:"bytes,3,opt,name=error_summary,json=errorSummary,proto3" json:"error_summary,omitempty"`
}

func (x *EnumerationResponse) Reset() {
	*x = EnumerationResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_steps_enumeration_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EnumerationResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EnumerationResponse) ProtoMessage() {}

func (x *EnumerationResponse) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_steps_enumeration_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EnumerationResponse.ProtoReflect.Descriptor instead.
func (*EnumerationResponse) Descriptor() ([]byte, []int) {
	return file_test_platform_steps_enumeration_proto_rawDescGZIP(), []int{3}
}

func (x *EnumerationResponse) GetAutotestInvocations() []*EnumerationResponse_AutotestInvocation {
	if x != nil {
		return x.AutotestInvocations
	}
	return nil
}

func (x *EnumerationResponse) GetErrorSummary() string {
	if x != nil {
		return x.ErrorSummary
	}
	return ""
}

type EnumerationResponse_AutotestInvocation struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Test *api.AutotestTest `protobuf:"bytes,1,opt,name=test,proto3" json:"test,omitempty"`
	// Passthrough arguments from test_platform.Request_Test
	TestArgs    string `protobuf:"bytes,2,opt,name=test_args,json=testArgs,proto3" json:"test_args,omitempty"`
	DisplayName string `protobuf:"bytes,3,opt,name=display_name,json=displayName,proto3" json:"display_name,omitempty"`
	// result_keyvals are autotest keyvals that should be included in the
	// results corresponding to this invocation.
	//
	// These keyvals are overridden by Request.Decorations.autotest_keyvals.
	ResultKeyvals map[string]string `protobuf:"bytes,4,rep,name=result_keyvals,json=resultKeyvals,proto3" json:"result_keyvals,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (x *EnumerationResponse_AutotestInvocation) Reset() {
	*x = EnumerationResponse_AutotestInvocation{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_steps_enumeration_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EnumerationResponse_AutotestInvocation) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EnumerationResponse_AutotestInvocation) ProtoMessage() {}

func (x *EnumerationResponse_AutotestInvocation) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_steps_enumeration_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EnumerationResponse_AutotestInvocation.ProtoReflect.Descriptor instead.
func (*EnumerationResponse_AutotestInvocation) Descriptor() ([]byte, []int) {
	return file_test_platform_steps_enumeration_proto_rawDescGZIP(), []int{3, 0}
}

func (x *EnumerationResponse_AutotestInvocation) GetTest() *api.AutotestTest {
	if x != nil {
		return x.Test
	}
	return nil
}

func (x *EnumerationResponse_AutotestInvocation) GetTestArgs() string {
	if x != nil {
		return x.TestArgs
	}
	return ""
}

func (x *EnumerationResponse_AutotestInvocation) GetDisplayName() string {
	if x != nil {
		return x.DisplayName
	}
	return ""
}

func (x *EnumerationResponse_AutotestInvocation) GetResultKeyvals() map[string]string {
	if x != nil {
		return x.ResultKeyvals
	}
	return nil
}

var File_test_platform_steps_enumeration_proto protoreflect.FileDescriptor

var file_test_platform_steps_enumeration_proto_rawDesc = []byte{
	0x0a, 0x25, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f,
	0x73, 0x74, 0x65, 0x70, 0x73, 0x2f, 0x65, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x13, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x1a, 0x20, 0x63, 0x68,
	0x72, 0x6f, 0x6d, 0x69, 0x74, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x5f,
	0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x72, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xad, 0x02, 0x0a, 0x13,
	0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x73, 0x12, 0x43, 0x0a, 0x08, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x18,
	0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x27, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e, 0x75, 0x6d,
	0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x08,
	0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x12, 0x65, 0x0a, 0x0f, 0x74, 0x61, 0x67, 0x67,
	0x65, 0x64, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x3c, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x2e, 0x54, 0x61, 0x67, 0x67,
	0x65, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52,
	0x0e, 0x74, 0x61, 0x67, 0x67, 0x65, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x1a,
	0x6a, 0x0a, 0x13, 0x54, 0x61, 0x67, 0x67, 0x65, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x3d, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x27, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70,
	0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e,
	0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0xb7, 0x02, 0x0a, 0x14,
	0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x73, 0x12, 0x46, 0x0a, 0x09, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x28, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70,
	0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e,
	0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x52, 0x09, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x12, 0x69, 0x0a, 0x10,
	0x74, 0x61, 0x67, 0x67, 0x65, 0x64, 0x5f, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73,
	0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x3e, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e, 0x75,
	0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x73, 0x2e, 0x54, 0x61, 0x67, 0x67, 0x65, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0f, 0x74, 0x61, 0x67, 0x67, 0x65, 0x64, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x1a, 0x6c, 0x0a, 0x14, 0x54, 0x61, 0x67, 0x67, 0x65,
	0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12,
	0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65,
	0x79, 0x12, 0x3e, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x28, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75,
	0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0x96, 0x01, 0x0a, 0x12, 0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x42, 0x0a, 0x08,
	0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x26,
	0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x50, 0x61, 0x72, 0x61, 0x6d, 0x73, 0x2e, 0x4d, 0x65,
	0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x52, 0x08, 0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61,
	0x12, 0x3c, 0x0a, 0x09, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x6e, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x54, 0x65, 0x73, 0x74,
	0x50, 0x6c, 0x61, 0x6e, 0x52, 0x08, 0x74, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x6e, 0x22, 0xea,
	0x03, 0x0a, 0x13, 0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x6e, 0x0a, 0x14, 0x61, 0x75, 0x74, 0x6f, 0x74, 0x65,
	0x73, 0x74, 0x5f, 0x69, 0x6e, 0x76, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x02,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x3b, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2e, 0x45, 0x6e, 0x75, 0x6d, 0x65,
	0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x2e, 0x41,
	0x75, 0x74, 0x6f, 0x74, 0x65, 0x73, 0x74, 0x49, 0x6e, 0x76, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x13, 0x61, 0x75, 0x74, 0x6f, 0x74, 0x65, 0x73, 0x74, 0x49, 0x6e, 0x76, 0x6f, 0x63,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x23, 0x0a, 0x0d, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x5f,
	0x73, 0x75, 0x6d, 0x6d, 0x61, 0x72, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x65,
	0x72, 0x72, 0x6f, 0x72, 0x53, 0x75, 0x6d, 0x6d, 0x61, 0x72, 0x79, 0x1a, 0xbd, 0x02, 0x0a, 0x12,
	0x41, 0x75, 0x74, 0x6f, 0x74, 0x65, 0x73, 0x74, 0x49, 0x6e, 0x76, 0x6f, 0x63, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x2e, 0x0a, 0x04, 0x74, 0x65, 0x73, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1a, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x74, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x41, 0x75, 0x74, 0x6f, 0x74, 0x65, 0x73, 0x74, 0x54, 0x65, 0x73, 0x74, 0x52, 0x04, 0x74, 0x65,
	0x73, 0x74, 0x12, 0x1b, 0x0a, 0x09, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x61, 0x72, 0x67, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x74, 0x65, 0x73, 0x74, 0x41, 0x72, 0x67, 0x73, 0x12,
	0x21, 0x0a, 0x0c, 0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x75, 0x0a, 0x0e, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x5f, 0x6b, 0x65, 0x79,
	0x76, 0x61, 0x6c, 0x73, 0x18, 0x04, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x4e, 0x2e, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70, 0x73,
	0x2e, 0x45, 0x6e, 0x75, 0x6d, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x2e, 0x41, 0x75, 0x74, 0x6f, 0x74, 0x65, 0x73, 0x74, 0x49, 0x6e, 0x76,
	0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x4b, 0x65,
	0x79, 0x76, 0x61, 0x6c, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0d, 0x72, 0x65, 0x73, 0x75,
	0x6c, 0x74, 0x4b, 0x65, 0x79, 0x76, 0x61, 0x6c, 0x73, 0x1a, 0x40, 0x0a, 0x12, 0x52, 0x65, 0x73,
	0x75, 0x6c, 0x74, 0x4b, 0x65, 0x79, 0x76, 0x61, 0x6c, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12,
	0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65,
	0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x42, 0x3f, 0x5a, 0x3d, 0x67,
	0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x63,
	0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x73, 0x74, 0x65, 0x70, 0x73, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_test_platform_steps_enumeration_proto_rawDescOnce sync.Once
	file_test_platform_steps_enumeration_proto_rawDescData = file_test_platform_steps_enumeration_proto_rawDesc
)

func file_test_platform_steps_enumeration_proto_rawDescGZIP() []byte {
	file_test_platform_steps_enumeration_proto_rawDescOnce.Do(func() {
		file_test_platform_steps_enumeration_proto_rawDescData = protoimpl.X.CompressGZIP(file_test_platform_steps_enumeration_proto_rawDescData)
	})
	return file_test_platform_steps_enumeration_proto_rawDescData
}

var file_test_platform_steps_enumeration_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_test_platform_steps_enumeration_proto_goTypes = []interface{}{
	(*EnumerationRequests)(nil),  // 0: test_platform.steps.EnumerationRequests
	(*EnumerationResponses)(nil), // 1: test_platform.steps.EnumerationResponses
	(*EnumerationRequest)(nil),   // 2: test_platform.steps.EnumerationRequest
	(*EnumerationResponse)(nil),  // 3: test_platform.steps.EnumerationResponse
	nil,                          // 4: test_platform.steps.EnumerationRequests.TaggedRequestsEntry
	nil,                          // 5: test_platform.steps.EnumerationResponses.TaggedResponsesEntry
	(*EnumerationResponse_AutotestInvocation)(nil), // 6: test_platform.steps.EnumerationResponse.AutotestInvocation
	nil, // 7: test_platform.steps.EnumerationResponse.AutotestInvocation.ResultKeyvalsEntry
	(*test_platform.Request_Params_Metadata)(nil), // 8: test_platform.Request.Params.Metadata
	(*test_platform.Request_TestPlan)(nil),        // 9: test_platform.Request.TestPlan
	(*api.AutotestTest)(nil),                      // 10: chromite.api.AutotestTest
}
var file_test_platform_steps_enumeration_proto_depIdxs = []int32{
	2,  // 0: test_platform.steps.EnumerationRequests.requests:type_name -> test_platform.steps.EnumerationRequest
	4,  // 1: test_platform.steps.EnumerationRequests.tagged_requests:type_name -> test_platform.steps.EnumerationRequests.TaggedRequestsEntry
	3,  // 2: test_platform.steps.EnumerationResponses.responses:type_name -> test_platform.steps.EnumerationResponse
	5,  // 3: test_platform.steps.EnumerationResponses.tagged_responses:type_name -> test_platform.steps.EnumerationResponses.TaggedResponsesEntry
	8,  // 4: test_platform.steps.EnumerationRequest.metadata:type_name -> test_platform.Request.Params.Metadata
	9,  // 5: test_platform.steps.EnumerationRequest.test_plan:type_name -> test_platform.Request.TestPlan
	6,  // 6: test_platform.steps.EnumerationResponse.autotest_invocations:type_name -> test_platform.steps.EnumerationResponse.AutotestInvocation
	2,  // 7: test_platform.steps.EnumerationRequests.TaggedRequestsEntry.value:type_name -> test_platform.steps.EnumerationRequest
	3,  // 8: test_platform.steps.EnumerationResponses.TaggedResponsesEntry.value:type_name -> test_platform.steps.EnumerationResponse
	10, // 9: test_platform.steps.EnumerationResponse.AutotestInvocation.test:type_name -> chromite.api.AutotestTest
	7,  // 10: test_platform.steps.EnumerationResponse.AutotestInvocation.result_keyvals:type_name -> test_platform.steps.EnumerationResponse.AutotestInvocation.ResultKeyvalsEntry
	11, // [11:11] is the sub-list for method output_type
	11, // [11:11] is the sub-list for method input_type
	11, // [11:11] is the sub-list for extension type_name
	11, // [11:11] is the sub-list for extension extendee
	0,  // [0:11] is the sub-list for field type_name
}

func init() { file_test_platform_steps_enumeration_proto_init() }
func file_test_platform_steps_enumeration_proto_init() {
	if File_test_platform_steps_enumeration_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_test_platform_steps_enumeration_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EnumerationRequests); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_steps_enumeration_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EnumerationResponses); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_steps_enumeration_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EnumerationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_steps_enumeration_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EnumerationResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_steps_enumeration_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EnumerationResponse_AutotestInvocation); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_test_platform_steps_enumeration_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_test_platform_steps_enumeration_proto_goTypes,
		DependencyIndexes: file_test_platform_steps_enumeration_proto_depIdxs,
		MessageInfos:      file_test_platform_steps_enumeration_proto_msgTypes,
	}.Build()
	File_test_platform_steps_enumeration_proto = out.File
	file_test_platform_steps_enumeration_proto_rawDesc = nil
	file_test_platform_steps_enumeration_proto_goTypes = nil
	file_test_platform_steps_enumeration_proto_depIdxs = nil
}
