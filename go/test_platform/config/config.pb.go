// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: test_platform/config/config.proto

package config

import (
	test_runner "go.chromium.org/chromiumos/infra/proto/go/test_platform/migration/test_runner"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Config defined configuration parameters of cros_test_platform.
type Config struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SkylabSwarming      *Config_Swarming     `protobuf:"bytes,1,opt,name=skylab_swarming,json=skylabSwarming,proto3" json:"skylab_swarming,omitempty"`
	SkylabIsolate       *Config_Isolate      `protobuf:"bytes,3,opt,name=skylab_isolate,json=skylabIsolate,proto3" json:"skylab_isolate,omitempty"`
	SkylabWorker        *Config_SkylabWorker `protobuf:"bytes,4,opt,name=skylab_worker,json=skylabWorker,proto3" json:"skylab_worker,omitempty"`
	Versioning          *Config_Versioning   `protobuf:"bytes,7,opt,name=versioning,proto3" json:"versioning,omitempty"`
	TestRunner          *Config_TestRunner   `protobuf:"bytes,8,opt,name=test_runner,json=testRunner,proto3" json:"test_runner,omitempty"`
	TestRunnerMigration *test_runner.Config  `protobuf:"bytes,9,opt,name=test_runner_migration,json=testRunnerMigration,proto3" json:"test_runner_migration,omitempty"`
	// TODO(crbug.com/1132489) Delete once builders stop using this.
	//
	// Deprecated: Do not use.
	Pubsub *Config_PubSub `protobuf:"bytes,10,opt,name=pubsub,proto3" json:"pubsub,omitempty"`
	// go/ctp-result-flow: CTP publishes a message with build ID, once
	// at build start and again at build end
	ResultFlowChannel *Config_PubSub `protobuf:"bytes,11,opt,name=result_flow_channel,json=resultFlowChannel,proto3" json:"result_flow_channel,omitempty"`
}

func (x *Config) Reset() {
	*x = Config{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config) ProtoMessage() {}

func (x *Config) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config.ProtoReflect.Descriptor instead.
func (*Config) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0}
}

func (x *Config) GetSkylabSwarming() *Config_Swarming {
	if x != nil {
		return x.SkylabSwarming
	}
	return nil
}

func (x *Config) GetSkylabIsolate() *Config_Isolate {
	if x != nil {
		return x.SkylabIsolate
	}
	return nil
}

func (x *Config) GetSkylabWorker() *Config_SkylabWorker {
	if x != nil {
		return x.SkylabWorker
	}
	return nil
}

func (x *Config) GetVersioning() *Config_Versioning {
	if x != nil {
		return x.Versioning
	}
	return nil
}

func (x *Config) GetTestRunner() *Config_TestRunner {
	if x != nil {
		return x.TestRunner
	}
	return nil
}

func (x *Config) GetTestRunnerMigration() *test_runner.Config {
	if x != nil {
		return x.TestRunnerMigration
	}
	return nil
}

// Deprecated: Do not use.
func (x *Config) GetPubsub() *Config_PubSub {
	if x != nil {
		return x.Pubsub
	}
	return nil
}

func (x *Config) GetResultFlowChannel() *Config_PubSub {
	if x != nil {
		return x.ResultFlowChannel
	}
	return nil
}

// Swarming defines configuration parameters related to a swarming instance.
type Config_Swarming struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Server       string `protobuf:"bytes,1,opt,name=server,proto3" json:"server,omitempty"`
	AuthJsonPath string `protobuf:"bytes,2,opt,name=auth_json_path,json=authJsonPath,proto3" json:"auth_json_path,omitempty"`
}

func (x *Config_Swarming) Reset() {
	*x = Config_Swarming{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_Swarming) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_Swarming) ProtoMessage() {}

func (x *Config_Swarming) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_Swarming.ProtoReflect.Descriptor instead.
func (*Config_Swarming) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 0}
}

func (x *Config_Swarming) GetServer() string {
	if x != nil {
		return x.Server
	}
	return ""
}

func (x *Config_Swarming) GetAuthJsonPath() string {
	if x != nil {
		return x.AuthJsonPath
	}
	return ""
}

// Isolate defines configuration parameters related to an isolate instance.
type Config_Isolate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AuthJsonPath string `protobuf:"bytes,1,opt,name=auth_json_path,json=authJsonPath,proto3" json:"auth_json_path,omitempty"`
}

func (x *Config_Isolate) Reset() {
	*x = Config_Isolate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_Isolate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_Isolate) ProtoMessage() {}

func (x *Config_Isolate) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_Isolate.ProtoReflect.Descriptor instead.
func (*Config_Isolate) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 1}
}

func (x *Config_Isolate) GetAuthJsonPath() string {
	if x != nil {
		return x.AuthJsonPath
	}
	return ""
}

// SkylabWorker defines configuration parameters related to
// skylab_swarming_worker.
type Config_SkylabWorker struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// LUCI project for tasks; e.g. "chromeos"
	LuciProject string `protobuf:"bytes,1,opt,name=luci_project,json=luciProject,proto3" json:"luci_project,omitempty"`
	// LogDog host for test task logs, e.g. luci-logdog.appspot.com
	LogDogHost string `protobuf:"bytes,2,opt,name=log_dog_host,json=logDogHost,proto3" json:"log_dog_host,omitempty"`
}

func (x *Config_SkylabWorker) Reset() {
	*x = Config_SkylabWorker{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_SkylabWorker) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_SkylabWorker) ProtoMessage() {}

func (x *Config_SkylabWorker) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_SkylabWorker.ProtoReflect.Descriptor instead.
func (*Config_SkylabWorker) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 2}
}

func (x *Config_SkylabWorker) GetLuciProject() string {
	if x != nil {
		return x.LuciProject
	}
	return ""
}

func (x *Config_SkylabWorker) GetLogDogHost() string {
	if x != nil {
		return x.LogDogHost
	}
	return ""
}

// Versioning defines configuration parameters dictating which versions of
// the downstream components to use.
type Config_Versioning struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	CrosTestPlatformBinary *Config_Versioning_CrosTestPlatformBinary `protobuf:"bytes,1,opt,name=cros_test_platform_binary,json=crosTestPlatformBinary,proto3" json:"cros_test_platform_binary,omitempty"`
}

func (x *Config_Versioning) Reset() {
	*x = Config_Versioning{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_Versioning) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_Versioning) ProtoMessage() {}

func (x *Config_Versioning) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_Versioning.ProtoReflect.Descriptor instead.
func (*Config_Versioning) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 3}
}

func (x *Config_Versioning) GetCrosTestPlatformBinary() *Config_Versioning_CrosTestPlatformBinary {
	if x != nil {
		return x.CrosTestPlatformBinary
	}
	return nil
}

// Buildbucket defines configuration parameters of a builder.
type Config_Buildbucket struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Buildbucket host, e.g. "cr-buildbucket.appspot.com".
	Host string `protobuf:"bytes,1,opt,name=host,proto3" json:"host,omitempty"`
	// Project+bucket+builder serves as a unique builder identifier, see
	// BuilderID in the build proto:
	// https://cs.chromium.org/search/?q=f:build.proto+BuilderID
	Project string `protobuf:"bytes,2,opt,name=project,proto3" json:"project,omitempty"`
	Bucket  string `protobuf:"bytes,3,opt,name=bucket,proto3" json:"bucket,omitempty"`
	Builder string `protobuf:"bytes,4,opt,name=builder,proto3" json:"builder,omitempty"`
}

func (x *Config_Buildbucket) Reset() {
	*x = Config_Buildbucket{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_Buildbucket) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_Buildbucket) ProtoMessage() {}

func (x *Config_Buildbucket) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_Buildbucket.ProtoReflect.Descriptor instead.
func (*Config_Buildbucket) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 4}
}

func (x *Config_Buildbucket) GetHost() string {
	if x != nil {
		return x.Host
	}
	return ""
}

func (x *Config_Buildbucket) GetProject() string {
	if x != nil {
		return x.Project
	}
	return ""
}

func (x *Config_Buildbucket) GetBucket() string {
	if x != nil {
		return x.Bucket
	}
	return ""
}

func (x *Config_Buildbucket) GetBuilder() string {
	if x != nil {
		return x.Builder
	}
	return ""
}

// PubSub defines the configuration parameters for PubSub topic.
type Config_PubSub struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Project string `protobuf:"bytes,1,opt,name=project,proto3" json:"project,omitempty"`
	Topic   string `protobuf:"bytes,2,opt,name=topic,proto3" json:"topic,omitempty"`
}

func (x *Config_PubSub) Reset() {
	*x = Config_PubSub{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_PubSub) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_PubSub) ProtoMessage() {}

func (x *Config_PubSub) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_PubSub.ProtoReflect.Descriptor instead.
func (*Config_PubSub) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 5}
}

func (x *Config_PubSub) GetProject() string {
	if x != nil {
		return x.Project
	}
	return ""
}

func (x *Config_PubSub) GetTopic() string {
	if x != nil {
		return x.Topic
	}
	return ""
}

// TestRunner defines configuration parameters related to the test_runner
// recipe.
type Config_TestRunner struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Buildbucket *Config_Buildbucket `protobuf:"bytes,1,opt,name=buildbucket,proto3" json:"buildbucket,omitempty"`
	// TODO(crbug.com/1132489) Delete once builders stop using this.
	//
	// Deprecated: Do not use.
	Pubsub *Config_PubSub `protobuf:"bytes,2,opt,name=pubsub,proto3" json:"pubsub,omitempty"`
	// go/ctp-result-flow: Trigger result flow processing of test runner
	// results.
	ResultFlowChannel *Config_PubSub `protobuf:"bytes,3,opt,name=result_flow_channel,json=resultFlowChannel,proto3" json:"result_flow_channel,omitempty"`
	// go/buildbucket-pubsub: Buildbucket status updates for child builds.
	BbStatusUpdateChannel *Config_PubSub `protobuf:"bytes,4,opt,name=bb_status_update_channel,json=bbStatusUpdateChannel,proto3" json:"bb_status_update_channel,omitempty"`
	// Which swarming pool the test_runner build will run in. Should match
	// the buildbucket configuration
	SwarmingPool string `protobuf:"bytes,5,opt,name=swarming_pool,json=swarmingPool,proto3" json:"swarming_pool,omitempty"`
}

func (x *Config_TestRunner) Reset() {
	*x = Config_TestRunner{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_TestRunner) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_TestRunner) ProtoMessage() {}

func (x *Config_TestRunner) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_TestRunner.ProtoReflect.Descriptor instead.
func (*Config_TestRunner) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 6}
}

func (x *Config_TestRunner) GetBuildbucket() *Config_Buildbucket {
	if x != nil {
		return x.Buildbucket
	}
	return nil
}

// Deprecated: Do not use.
func (x *Config_TestRunner) GetPubsub() *Config_PubSub {
	if x != nil {
		return x.Pubsub
	}
	return nil
}

func (x *Config_TestRunner) GetResultFlowChannel() *Config_PubSub {
	if x != nil {
		return x.ResultFlowChannel
	}
	return nil
}

func (x *Config_TestRunner) GetBbStatusUpdateChannel() *Config_PubSub {
	if x != nil {
		return x.BbStatusUpdateChannel
	}
	return nil
}

func (x *Config_TestRunner) GetSwarmingPool() string {
	if x != nil {
		return x.SwarmingPool
	}
	return ""
}

// CrosTestPlatformBinary describes which cros_test_platform version to
// use.
type Config_Versioning_CrosTestPlatformBinary struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Use the CIPD version with this label.
	CipdLabel string `protobuf:"bytes,1,opt,name=cipd_label,json=cipdLabel,proto3" json:"cipd_label,omitempty"`
}

func (x *Config_Versioning_CrosTestPlatformBinary) Reset() {
	*x = Config_Versioning_CrosTestPlatformBinary{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_config_config_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Config_Versioning_CrosTestPlatformBinary) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Config_Versioning_CrosTestPlatformBinary) ProtoMessage() {}

func (x *Config_Versioning_CrosTestPlatformBinary) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_config_config_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Config_Versioning_CrosTestPlatformBinary.ProtoReflect.Descriptor instead.
func (*Config_Versioning_CrosTestPlatformBinary) Descriptor() ([]byte, []int) {
	return file_test_platform_config_config_proto_rawDescGZIP(), []int{0, 3, 0}
}

func (x *Config_Versioning_CrosTestPlatformBinary) GetCipdLabel() string {
	if x != nil {
		return x.CipdLabel
	}
	return ""
}

var File_test_platform_config_config_proto protoreflect.FileDescriptor

var file_test_platform_config_config_proto_rawDesc = []byte{
	0x0a, 0x21, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f,
	0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x14, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x1a, 0x30, 0x74, 0x65, 0x73, 0x74, 0x5f,
	0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x6d, 0x69, 0x67, 0x72, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2f, 0x63,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xc1, 0x0c, 0x0a, 0x06,
	0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x4e, 0x0a, 0x0f, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62,
	0x5f, 0x73, 0x77, 0x61, 0x72, 0x6d, 0x69, 0x6e, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x25, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e,
	0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x53, 0x77,
	0x61, 0x72, 0x6d, 0x69, 0x6e, 0x67, 0x52, 0x0e, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x53, 0x77,
	0x61, 0x72, 0x6d, 0x69, 0x6e, 0x67, 0x12, 0x4b, 0x0a, 0x0e, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62,
	0x5f, 0x69, 0x73, 0x6f, 0x6c, 0x61, 0x74, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x24,
	0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x49, 0x73, 0x6f,
	0x6c, 0x61, 0x74, 0x65, 0x52, 0x0d, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x49, 0x73, 0x6f, 0x6c,
	0x61, 0x74, 0x65, 0x12, 0x4e, 0x0a, 0x0d, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x77, 0x6f,
	0x72, 0x6b, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69,
	0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x53, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x57,
	0x6f, 0x72, 0x6b, 0x65, 0x72, 0x52, 0x0c, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x57, 0x6f, 0x72,
	0x6b, 0x65, 0x72, 0x12, 0x47, 0x0a, 0x0a, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x69, 0x6e,
	0x67, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x27, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70,
	0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x69, 0x6e, 0x67,
	0x52, 0x0a, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x69, 0x6e, 0x67, 0x12, 0x48, 0x0a, 0x0b,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x18, 0x08, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x27, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e,
	0x54, 0x65, 0x73, 0x74, 0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x52, 0x0a, 0x74, 0x65, 0x73, 0x74,
	0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x12, 0x5f, 0x0a, 0x15, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x72,
	0x75, 0x6e, 0x6e, 0x65, 0x72, 0x5f, 0x6d, 0x69, 0x67, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2b, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x6d, 0x69, 0x67, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x72, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x2e, 0x43, 0x6f, 0x6e, 0x66,
	0x69, 0x67, 0x52, 0x13, 0x74, 0x65, 0x73, 0x74, 0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x4d, 0x69,
	0x67, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x3f, 0x0a, 0x06, 0x70, 0x75, 0x62, 0x73, 0x75,
	0x62, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70,
	0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x50, 0x75, 0x62, 0x53, 0x75, 0x62, 0x42, 0x02, 0x18, 0x01,
	0x52, 0x06, 0x70, 0x75, 0x62, 0x73, 0x75, 0x62, 0x12, 0x53, 0x0a, 0x13, 0x72, 0x65, 0x73, 0x75,
	0x6c, 0x74, 0x5f, 0x66, 0x6c, 0x6f, 0x77, 0x5f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x18,
	0x0b, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e,
	0x66, 0x69, 0x67, 0x2e, 0x50, 0x75, 0x62, 0x53, 0x75, 0x62, 0x52, 0x11, 0x72, 0x65, 0x73, 0x75,
	0x6c, 0x74, 0x46, 0x6c, 0x6f, 0x77, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x1a, 0x48, 0x0a,
	0x08, 0x53, 0x77, 0x61, 0x72, 0x6d, 0x69, 0x6e, 0x67, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x72, 0x76, 0x65,
	0x72, 0x12, 0x24, 0x0a, 0x0e, 0x61, 0x75, 0x74, 0x68, 0x5f, 0x6a, 0x73, 0x6f, 0x6e, 0x5f, 0x70,
	0x61, 0x74, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x75, 0x74, 0x68, 0x4a,
	0x73, 0x6f, 0x6e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x2f, 0x0a, 0x07, 0x49, 0x73, 0x6f, 0x6c, 0x61,
	0x74, 0x65, 0x12, 0x24, 0x0a, 0x0e, 0x61, 0x75, 0x74, 0x68, 0x5f, 0x6a, 0x73, 0x6f, 0x6e, 0x5f,
	0x70, 0x61, 0x74, 0x68, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x75, 0x74, 0x68,
	0x4a, 0x73, 0x6f, 0x6e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x53, 0x0a, 0x0c, 0x53, 0x6b, 0x79, 0x6c,
	0x61, 0x62, 0x57, 0x6f, 0x72, 0x6b, 0x65, 0x72, 0x12, 0x21, 0x0a, 0x0c, 0x6c, 0x75, 0x63, 0x69,
	0x5f, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x6c, 0x75, 0x63, 0x69, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x12, 0x20, 0x0a, 0x0c, 0x6c,
	0x6f, 0x67, 0x5f, 0x64, 0x6f, 0x67, 0x5f, 0x68, 0x6f, 0x73, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0a, 0x6c, 0x6f, 0x67, 0x44, 0x6f, 0x67, 0x48, 0x6f, 0x73, 0x74, 0x1a, 0xc0, 0x01,
	0x0a, 0x0a, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x69, 0x6e, 0x67, 0x12, 0x79, 0x0a, 0x19,
	0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x5f, 0x62, 0x69, 0x6e, 0x61, 0x72, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x3e, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e,
	0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x56, 0x65,
	0x72, 0x73, 0x69, 0x6f, 0x6e, 0x69, 0x6e, 0x67, 0x2e, 0x43, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73,
	0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x42, 0x69, 0x6e, 0x61, 0x72, 0x79, 0x52,
	0x16, 0x63, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x42, 0x69, 0x6e, 0x61, 0x72, 0x79, 0x1a, 0x37, 0x0a, 0x16, 0x43, 0x72, 0x6f, 0x73, 0x54,
	0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x42, 0x69, 0x6e, 0x61, 0x72,
	0x79, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x69, 0x70, 0x64, 0x5f, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x69, 0x70, 0x64, 0x4c, 0x61, 0x62, 0x65, 0x6c,
	0x1a, 0x6d, 0x0a, 0x0b, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x12,
	0x12, 0x0a, 0x04, 0x68, 0x6f, 0x73, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x68,
	0x6f, 0x73, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x12, 0x16, 0x0a,
	0x06, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62,
	0x75, 0x63, 0x6b, 0x65, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x65, 0x72,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x65, 0x72, 0x1a,
	0x38, 0x0a, 0x06, 0x50, 0x75, 0x62, 0x53, 0x75, 0x62, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x72, 0x6f,
	0x6a, 0x65, 0x63, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x70, 0x72, 0x6f, 0x6a,
	0x65, 0x63, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x70, 0x69, 0x63, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x05, 0x74, 0x6f, 0x70, 0x69, 0x63, 0x1a, 0xf1, 0x02, 0x0a, 0x0a, 0x54, 0x65,
	0x73, 0x74, 0x52, 0x75, 0x6e, 0x6e, 0x65, 0x72, 0x12, 0x4a, 0x0a, 0x0b, 0x62, 0x75, 0x69, 0x6c,
	0x64, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x28, 0x2e,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f,
	0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x42, 0x75, 0x69, 0x6c,
	0x64, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x52, 0x0b, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x62, 0x75,
	0x63, 0x6b, 0x65, 0x74, 0x12, 0x3f, 0x0a, 0x06, 0x70, 0x75, 0x62, 0x73, 0x75, 0x62, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66,
	0x69, 0x67, 0x2e, 0x50, 0x75, 0x62, 0x53, 0x75, 0x62, 0x42, 0x02, 0x18, 0x01, 0x52, 0x06, 0x70,
	0x75, 0x62, 0x73, 0x75, 0x62, 0x12, 0x53, 0x0a, 0x13, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x5f,
	0x66, 0x6c, 0x6f, 0x77, 0x5f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x23, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67,
	0x2e, 0x50, 0x75, 0x62, 0x53, 0x75, 0x62, 0x52, 0x11, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x46,
	0x6c, 0x6f, 0x77, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x5c, 0x0a, 0x18, 0x62, 0x62,
	0x5f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x5f, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x5f, 0x63,
	0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e,
	0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x50, 0x75, 0x62, 0x53, 0x75,
	0x62, 0x52, 0x15, 0x62, 0x62, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x23, 0x0a, 0x0d, 0x73, 0x77, 0x61, 0x72,
	0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x6f, 0x6f, 0x6c, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0c, 0x73, 0x77, 0x61, 0x72, 0x6d, 0x69, 0x6e, 0x67, 0x50, 0x6f, 0x6f, 0x6c, 0x4a, 0x04, 0x08,
	0x02, 0x10, 0x03, 0x4a, 0x04, 0x08, 0x05, 0x10, 0x06, 0x4a, 0x04, 0x08, 0x06, 0x10, 0x07, 0x42,
	0x40, 0x5a, 0x3e, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f,
	0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69, 0x6e,
	0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x63, 0x6f, 0x6e, 0x66, 0x69,
	0x67, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_test_platform_config_config_proto_rawDescOnce sync.Once
	file_test_platform_config_config_proto_rawDescData = file_test_platform_config_config_proto_rawDesc
)

func file_test_platform_config_config_proto_rawDescGZIP() []byte {
	file_test_platform_config_config_proto_rawDescOnce.Do(func() {
		file_test_platform_config_config_proto_rawDescData = protoimpl.X.CompressGZIP(file_test_platform_config_config_proto_rawDescData)
	})
	return file_test_platform_config_config_proto_rawDescData
}

var file_test_platform_config_config_proto_msgTypes = make([]protoimpl.MessageInfo, 9)
var file_test_platform_config_config_proto_goTypes = []interface{}{
	(*Config)(nil),                                   // 0: test_platform.config.Config
	(*Config_Swarming)(nil),                          // 1: test_platform.config.Config.Swarming
	(*Config_Isolate)(nil),                           // 2: test_platform.config.Config.Isolate
	(*Config_SkylabWorker)(nil),                      // 3: test_platform.config.Config.SkylabWorker
	(*Config_Versioning)(nil),                        // 4: test_platform.config.Config.Versioning
	(*Config_Buildbucket)(nil),                       // 5: test_platform.config.Config.Buildbucket
	(*Config_PubSub)(nil),                            // 6: test_platform.config.Config.PubSub
	(*Config_TestRunner)(nil),                        // 7: test_platform.config.Config.TestRunner
	(*Config_Versioning_CrosTestPlatformBinary)(nil), // 8: test_platform.config.Config.Versioning.CrosTestPlatformBinary
	(*test_runner.Config)(nil),                       // 9: test_platform.migration.test_runner.Config
}
var file_test_platform_config_config_proto_depIdxs = []int32{
	1,  // 0: test_platform.config.Config.skylab_swarming:type_name -> test_platform.config.Config.Swarming
	2,  // 1: test_platform.config.Config.skylab_isolate:type_name -> test_platform.config.Config.Isolate
	3,  // 2: test_platform.config.Config.skylab_worker:type_name -> test_platform.config.Config.SkylabWorker
	4,  // 3: test_platform.config.Config.versioning:type_name -> test_platform.config.Config.Versioning
	7,  // 4: test_platform.config.Config.test_runner:type_name -> test_platform.config.Config.TestRunner
	9,  // 5: test_platform.config.Config.test_runner_migration:type_name -> test_platform.migration.test_runner.Config
	6,  // 6: test_platform.config.Config.pubsub:type_name -> test_platform.config.Config.PubSub
	6,  // 7: test_platform.config.Config.result_flow_channel:type_name -> test_platform.config.Config.PubSub
	8,  // 8: test_platform.config.Config.Versioning.cros_test_platform_binary:type_name -> test_platform.config.Config.Versioning.CrosTestPlatformBinary
	5,  // 9: test_platform.config.Config.TestRunner.buildbucket:type_name -> test_platform.config.Config.Buildbucket
	6,  // 10: test_platform.config.Config.TestRunner.pubsub:type_name -> test_platform.config.Config.PubSub
	6,  // 11: test_platform.config.Config.TestRunner.result_flow_channel:type_name -> test_platform.config.Config.PubSub
	6,  // 12: test_platform.config.Config.TestRunner.bb_status_update_channel:type_name -> test_platform.config.Config.PubSub
	13, // [13:13] is the sub-list for method output_type
	13, // [13:13] is the sub-list for method input_type
	13, // [13:13] is the sub-list for extension type_name
	13, // [13:13] is the sub-list for extension extendee
	0,  // [0:13] is the sub-list for field type_name
}

func init() { file_test_platform_config_config_proto_init() }
func file_test_platform_config_config_proto_init() {
	if File_test_platform_config_config_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_test_platform_config_config_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_Swarming); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_Isolate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_SkylabWorker); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_Versioning); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_Buildbucket); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_PubSub); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_TestRunner); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_test_platform_config_config_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Config_Versioning_CrosTestPlatformBinary); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_test_platform_config_config_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   9,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_test_platform_config_config_proto_goTypes,
		DependencyIndexes: file_test_platform_config_config_proto_depIdxs,
		MessageInfos:      file_test_platform_config_config_proto_msgTypes,
	}.Build()
	File_test_platform_config_config_proto = out.File
	file_test_platform_config_config_proto_rawDesc = nil
	file_test_platform_config_config_proto_goTypes = nil
	file_test_platform_config_config_proto_depIdxs = nil
}
