// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: test_platform/cros_test_platform/properties.proto

package cros_test_platform

import (
	api "go.chromium.org/chromiumos/config/go/test/api"
	test_platform "go.chromium.org/chromiumos/infra/proto/go/test_platform"
	config "go.chromium.org/chromiumos/infra/proto/go/test_platform/config"
	steps "go.chromium.org/chromiumos/infra/proto/go/test_platform/steps"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CrosTestPlatformProperties_LUCIExperiments int32

const (
	CrosTestPlatformProperties_SUITE_EXECUTION_LIMIT CrosTestPlatformProperties_LUCIExperiments = 0
)

// Enum value maps for CrosTestPlatformProperties_LUCIExperiments.
var (
	CrosTestPlatformProperties_LUCIExperiments_name = map[int32]string{
		0: "SUITE_EXECUTION_LIMIT",
	}
	CrosTestPlatformProperties_LUCIExperiments_value = map[string]int32{
		"SUITE_EXECUTION_LIMIT": 0,
	}
)

func (x CrosTestPlatformProperties_LUCIExperiments) Enum() *CrosTestPlatformProperties_LUCIExperiments {
	p := new(CrosTestPlatformProperties_LUCIExperiments)
	*p = x
	return p
}

func (x CrosTestPlatformProperties_LUCIExperiments) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (CrosTestPlatformProperties_LUCIExperiments) Descriptor() protoreflect.EnumDescriptor {
	return file_test_platform_cros_test_platform_properties_proto_enumTypes[0].Descriptor()
}

func (CrosTestPlatformProperties_LUCIExperiments) Type() protoreflect.EnumType {
	return &file_test_platform_cros_test_platform_properties_proto_enumTypes[0]
}

func (x CrosTestPlatformProperties_LUCIExperiments) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use CrosTestPlatformProperties_LUCIExperiments.Descriptor instead.
func (CrosTestPlatformProperties_LUCIExperiments) EnumDescriptor() ([]byte, []int) {
	return file_test_platform_cros_test_platform_properties_proto_rawDescGZIP(), []int{0, 0}
}

type CrosTestPlatformProperties struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// request describes a request to the cros_test_platform recipe, as
	// described by a caller.
	//
	// Exactly one of request or requests must be set.
	Request *test_platform.Request `protobuf:"bytes,2,opt,name=request,proto3" json:"request,omitempty"`
	// requests describes a set of requests to the cros_test_platform recipe, as
	// described by a caller. The key in the dictionary is an opaque tag used to
	// group the responses for the individual requests.
	//
	// Exactly one of request or requests must be set.
	Requests map[string]*test_platform.Request `protobuf:"bytes,5,rep,name=requests,proto3" json:"requests,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	// config describes configuration information for the cros_test_platform
	// recipe.
	//
	// This property should not be specified by callers, but instead set by
	// builder properties.
	Config *config.Config `protobuf:"bytes,3,opt,name=config,proto3" json:"config,omitempty"`
	// response describes the response from a cros_test_platform recipe.
	// response is only set for builds with a single request.
	//
	// This property is set by the recipe execution, not by the caller.
	//
	// Deprecated: Do not use.
	Response *steps.ExecuteResponse `protobuf:"bytes,4,opt,name=response,proto3" json:"response,omitempty"`
	// responses describes the responses from a cros_test_platform recipe.
	//
	// This property is set by the recipe execution, not by the caller.
	//
	// Deprecated: Do not use.
	Responses map[string]*steps.ExecuteResponse `protobuf:"bytes,6,rep,name=responses,proto3" json:"responses,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	// This consists of an ExecuteResponses proto, compressed and converted to
	// base64 encoding for safe transmission as a string.
	CompressedResponses string `protobuf:"bytes,8,opt,name=compressed_responses,json=compressedResponses,proto3" json:"compressed_responses,omitempty"`
	// This consists of an ExecuteResponses proto in JSON format, compressed and
	// converted to base64 encoding for safe transmission as a string. It is an
	// interim measure for switching off the responses field, which has become
	// an urgent problem for message size. See crbug.com/1073551
	//
	// Deprecated: Do not use.
	CompressedJsonResponses string `protobuf:"bytes,9,opt,name=compressed_json_responses,json=compressedJsonResponses,proto3" json:"compressed_json_responses,omitempty"`
	// This recipe exports to ResultDB by default only if the build is a top-level
	// build (i.e. it has no parent build). The following prop forces the recipe
	// to ignore the ancestor check, and export no matter what. Defaults to false.
	ForceExport bool `protobuf:"varint,10,opt,name=force_export,json=forceExport,proto3" json:"force_export,omitempty"`
	// This field is used for LUCI builder experiments defined in infra/config.
	Experiments []CrosTestPlatformProperties_LUCIExperiments `protobuf:"varint,11,rep,packed,name=experiments,proto3,enum=test_platform.cros_test_platform.CrosTestPlatformProperties_LUCIExperiments" json:"experiments,omitempty"`
	// The following property used to determine the build configs are for
	// parnters. Defaults to false.
	PartnerConfig bool `protobuf:"varint,12,opt,name=partner_config,json=partnerConfig,proto3" json:"partner_config,omitempty"`
	// New CTPv2 request. If this is provided, ctpv1 flow will be bypassed and
	// CTPv2 binary will be invoked from recipes.
	Ctpv2Request *api.CTPv2Request `protobuf:"bytes,13,opt,name=ctpv2_request,json=ctpv2Request,proto3" json:"ctpv2_request,omitempty"`
}

func (x *CrosTestPlatformProperties) Reset() {
	*x = CrosTestPlatformProperties{}
	if protoimpl.UnsafeEnabled {
		mi := &file_test_platform_cros_test_platform_properties_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CrosTestPlatformProperties) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CrosTestPlatformProperties) ProtoMessage() {}

func (x *CrosTestPlatformProperties) ProtoReflect() protoreflect.Message {
	mi := &file_test_platform_cros_test_platform_properties_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CrosTestPlatformProperties.ProtoReflect.Descriptor instead.
func (*CrosTestPlatformProperties) Descriptor() ([]byte, []int) {
	return file_test_platform_cros_test_platform_properties_proto_rawDescGZIP(), []int{0}
}

func (x *CrosTestPlatformProperties) GetRequest() *test_platform.Request {
	if x != nil {
		return x.Request
	}
	return nil
}

func (x *CrosTestPlatformProperties) GetRequests() map[string]*test_platform.Request {
	if x != nil {
		return x.Requests
	}
	return nil
}

func (x *CrosTestPlatformProperties) GetConfig() *config.Config {
	if x != nil {
		return x.Config
	}
	return nil
}

// Deprecated: Do not use.
func (x *CrosTestPlatformProperties) GetResponse() *steps.ExecuteResponse {
	if x != nil {
		return x.Response
	}
	return nil
}

// Deprecated: Do not use.
func (x *CrosTestPlatformProperties) GetResponses() map[string]*steps.ExecuteResponse {
	if x != nil {
		return x.Responses
	}
	return nil
}

func (x *CrosTestPlatformProperties) GetCompressedResponses() string {
	if x != nil {
		return x.CompressedResponses
	}
	return ""
}

// Deprecated: Do not use.
func (x *CrosTestPlatformProperties) GetCompressedJsonResponses() string {
	if x != nil {
		return x.CompressedJsonResponses
	}
	return ""
}

func (x *CrosTestPlatformProperties) GetForceExport() bool {
	if x != nil {
		return x.ForceExport
	}
	return false
}

func (x *CrosTestPlatformProperties) GetExperiments() []CrosTestPlatformProperties_LUCIExperiments {
	if x != nil {
		return x.Experiments
	}
	return nil
}

func (x *CrosTestPlatformProperties) GetPartnerConfig() bool {
	if x != nil {
		return x.PartnerConfig
	}
	return false
}

func (x *CrosTestPlatformProperties) GetCtpv2Request() *api.CTPv2Request {
	if x != nil {
		return x.Ctpv2Request
	}
	return nil
}

var File_test_platform_cros_test_platform_properties_proto protoreflect.FileDescriptor

var file_test_platform_cros_test_platform_properties_proto_rawDesc = []byte{
	0x0a, 0x31, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f,
	0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x2f, 0x70, 0x72, 0x6f, 0x70, 0x65, 0x72, 0x74, 0x69, 0x65, 0x73, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x20, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x1a, 0x1b, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x1a, 0x23, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x2f, 0x73, 0x74, 0x65, 0x70, 0x73, 0x2f, 0x65, 0x78, 0x65, 0x63, 0x75, 0x74, 0x69, 0x6f,
	0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x21, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2f, 0x63, 0x6f,
	0x6e, 0x66, 0x69, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1e, 0x63, 0x68, 0x72, 0x6f,
	0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f,
	0x63, 0x74, 0x70, 0x32, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x83, 0x08, 0x0a, 0x1a, 0x43,
	0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50,
	0x72, 0x6f, 0x70, 0x65, 0x72, 0x74, 0x69, 0x65, 0x73, 0x12, 0x30, 0x0a, 0x07, 0x72, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x74, 0x65, 0x73,
	0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x52, 0x07, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x66, 0x0a, 0x08, 0x72,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x18, 0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x4a, 0x2e,
	0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x72,
	0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x2e, 0x43, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x50, 0x72, 0x6f, 0x70, 0x65, 0x72, 0x74, 0x69, 0x65, 0x73, 0x2e, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x08, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x73, 0x12, 0x34, 0x0a, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69,
	0x67, 0x52, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x44, 0x0a, 0x08, 0x72, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x24, 0x2e, 0x74, 0x65,
	0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70,
	0x73, 0x2e, 0x45, 0x78, 0x65, 0x63, 0x75, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x42, 0x02, 0x18, 0x01, 0x52, 0x08, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x6d, 0x0a, 0x09, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x18, 0x06, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x4b, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x43, 0x72, 0x6f, 0x73, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50, 0x72, 0x6f, 0x70, 0x65, 0x72, 0x74, 0x69, 0x65, 0x73,
	0x2e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x42,
	0x02, 0x18, 0x01, 0x52, 0x09, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x12, 0x31,
	0x0a, 0x14, 0x63, 0x6f, 0x6d, 0x70, 0x72, 0x65, 0x73, 0x73, 0x65, 0x64, 0x5f, 0x72, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x13, 0x63, 0x6f,
	0x6d, 0x70, 0x72, 0x65, 0x73, 0x73, 0x65, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x73, 0x12, 0x3e, 0x0a, 0x19, 0x63, 0x6f, 0x6d, 0x70, 0x72, 0x65, 0x73, 0x73, 0x65, 0x64, 0x5f,
	0x6a, 0x73, 0x6f, 0x6e, 0x5f, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x18, 0x09,
	0x20, 0x01, 0x28, 0x09, 0x42, 0x02, 0x18, 0x01, 0x52, 0x17, 0x63, 0x6f, 0x6d, 0x70, 0x72, 0x65,
	0x73, 0x73, 0x65, 0x64, 0x4a, 0x73, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x73, 0x12, 0x21, 0x0a, 0x0c, 0x66, 0x6f, 0x72, 0x63, 0x65, 0x5f, 0x65, 0x78, 0x70, 0x6f, 0x72,
	0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0b, 0x66, 0x6f, 0x72, 0x63, 0x65, 0x45, 0x78,
	0x70, 0x6f, 0x72, 0x74, 0x12, 0x6e, 0x0a, 0x0b, 0x65, 0x78, 0x70, 0x65, 0x72, 0x69, 0x6d, 0x65,
	0x6e, 0x74, 0x73, 0x18, 0x0b, 0x20, 0x03, 0x28, 0x0e, 0x32, 0x4c, 0x2e, 0x74, 0x65, 0x73, 0x74,
	0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x5f, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x43, 0x72, 0x6f,
	0x73, 0x54, 0x65, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x50, 0x72, 0x6f,
	0x70, 0x65, 0x72, 0x74, 0x69, 0x65, 0x73, 0x2e, 0x4c, 0x55, 0x43, 0x49, 0x45, 0x78, 0x70, 0x65,
	0x72, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x52, 0x0b, 0x65, 0x78, 0x70, 0x65, 0x72, 0x69, 0x6d,
	0x65, 0x6e, 0x74, 0x73, 0x12, 0x25, 0x0a, 0x0e, 0x70, 0x61, 0x72, 0x74, 0x6e, 0x65, 0x72, 0x5f,
	0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0d, 0x70, 0x61,
	0x72, 0x74, 0x6e, 0x65, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x46, 0x0a, 0x0d, 0x63,
	0x74, 0x70, 0x76, 0x32, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x0d, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x21, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2e,
	0x74, 0x65, 0x73, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x43, 0x54, 0x50, 0x76, 0x32, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x0c, 0x63, 0x74, 0x70, 0x76, 0x32, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x53, 0x0a, 0x0d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x45,
	0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x2c, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x05, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x1a, 0x62, 0x0a, 0x0e, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65,
	0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x3a, 0x0a, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x24, 0x2e, 0x74, 0x65,
	0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x73, 0x74, 0x65, 0x70,
	0x73, 0x2e, 0x45, 0x78, 0x65, 0x63, 0x75, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0x2c, 0x0a, 0x0f,
	0x4c, 0x55, 0x43, 0x49, 0x45, 0x78, 0x70, 0x65, 0x72, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x12,
	0x19, 0x0a, 0x15, 0x53, 0x55, 0x49, 0x54, 0x45, 0x5f, 0x45, 0x58, 0x45, 0x43, 0x55, 0x54, 0x49,
	0x4f, 0x4e, 0x5f, 0x4c, 0x49, 0x4d, 0x49, 0x54, 0x10, 0x00, 0x4a, 0x04, 0x08, 0x01, 0x10, 0x02,
	0x42, 0x4c, 0x5a, 0x4a, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e,
	0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69,
	0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x74, 0x65,
	0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x63, 0x72, 0x6f, 0x73,
	0x5f, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_test_platform_cros_test_platform_properties_proto_rawDescOnce sync.Once
	file_test_platform_cros_test_platform_properties_proto_rawDescData = file_test_platform_cros_test_platform_properties_proto_rawDesc
)

func file_test_platform_cros_test_platform_properties_proto_rawDescGZIP() []byte {
	file_test_platform_cros_test_platform_properties_proto_rawDescOnce.Do(func() {
		file_test_platform_cros_test_platform_properties_proto_rawDescData = protoimpl.X.CompressGZIP(file_test_platform_cros_test_platform_properties_proto_rawDescData)
	})
	return file_test_platform_cros_test_platform_properties_proto_rawDescData
}

var file_test_platform_cros_test_platform_properties_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_test_platform_cros_test_platform_properties_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_test_platform_cros_test_platform_properties_proto_goTypes = []interface{}{
	(CrosTestPlatformProperties_LUCIExperiments)(0), // 0: test_platform.cros_test_platform.CrosTestPlatformProperties.LUCIExperiments
	(*CrosTestPlatformProperties)(nil),              // 1: test_platform.cros_test_platform.CrosTestPlatformProperties
	nil,                                             // 2: test_platform.cros_test_platform.CrosTestPlatformProperties.RequestsEntry
	nil,                                             // 3: test_platform.cros_test_platform.CrosTestPlatformProperties.ResponsesEntry
	(*test_platform.Request)(nil),                   // 4: test_platform.Request
	(*config.Config)(nil),                           // 5: test_platform.config.Config
	(*steps.ExecuteResponse)(nil),                   // 6: test_platform.steps.ExecuteResponse
	(*api.CTPv2Request)(nil),                        // 7: chromiumos.test.api.CTPv2Request
}
var file_test_platform_cros_test_platform_properties_proto_depIdxs = []int32{
	4, // 0: test_platform.cros_test_platform.CrosTestPlatformProperties.request:type_name -> test_platform.Request
	2, // 1: test_platform.cros_test_platform.CrosTestPlatformProperties.requests:type_name -> test_platform.cros_test_platform.CrosTestPlatformProperties.RequestsEntry
	5, // 2: test_platform.cros_test_platform.CrosTestPlatformProperties.config:type_name -> test_platform.config.Config
	6, // 3: test_platform.cros_test_platform.CrosTestPlatformProperties.response:type_name -> test_platform.steps.ExecuteResponse
	3, // 4: test_platform.cros_test_platform.CrosTestPlatformProperties.responses:type_name -> test_platform.cros_test_platform.CrosTestPlatformProperties.ResponsesEntry
	0, // 5: test_platform.cros_test_platform.CrosTestPlatformProperties.experiments:type_name -> test_platform.cros_test_platform.CrosTestPlatformProperties.LUCIExperiments
	7, // 6: test_platform.cros_test_platform.CrosTestPlatformProperties.ctpv2_request:type_name -> chromiumos.test.api.CTPv2Request
	4, // 7: test_platform.cros_test_platform.CrosTestPlatformProperties.RequestsEntry.value:type_name -> test_platform.Request
	6, // 8: test_platform.cros_test_platform.CrosTestPlatformProperties.ResponsesEntry.value:type_name -> test_platform.steps.ExecuteResponse
	9, // [9:9] is the sub-list for method output_type
	9, // [9:9] is the sub-list for method input_type
	9, // [9:9] is the sub-list for extension type_name
	9, // [9:9] is the sub-list for extension extendee
	0, // [0:9] is the sub-list for field type_name
}

func init() { file_test_platform_cros_test_platform_properties_proto_init() }
func file_test_platform_cros_test_platform_properties_proto_init() {
	if File_test_platform_cros_test_platform_properties_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_test_platform_cros_test_platform_properties_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CrosTestPlatformProperties); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_test_platform_cros_test_platform_properties_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_test_platform_cros_test_platform_properties_proto_goTypes,
		DependencyIndexes: file_test_platform_cros_test_platform_properties_proto_depIdxs,
		EnumInfos:         file_test_platform_cros_test_platform_properties_proto_enumTypes,
		MessageInfos:      file_test_platform_cros_test_platform_properties_proto_msgTypes,
	}.Build()
	File_test_platform_cros_test_platform_properties_proto = out.File
	file_test_platform_cros_test_platform_properties_proto_rawDesc = nil
	file_test_platform_cros_test_platform_properties_proto_goTypes = nil
	file_test_platform_cros_test_platform_properties_proto_depIdxs = nil
}
