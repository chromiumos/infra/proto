// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: testplans/board_priorities.proto

package testplans

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Configures how different groups of DUTs are chosen during test planning.
// Often different test plans can satisfy a test requirement, these priorities
// are used to choose a plan. Usually based off factors such as DUT
// availability.
type BoardPriority struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Key to identify a group of DUTs. Must align with keys used as
	// HwTest.skylab_board, see that field for more detailed definition of this
	// field.
	SkylabBoard string `protobuf:"bytes,1,opt,name=skylab_board,json=skylabBoard,proto3" json:"skylab_board,omitempty"`
	// Relative preference for a board. A board with a lower priority will be
	// chosen over a board with a higher priority. Priorities may be negative.
	// If a board does not have a BoardPriority, it is implicitly 0.
	Priority int32 `protobuf:"varint,2,opt,name=priority,proto3" json:"priority,omitempty"`
}

func (x *BoardPriority) Reset() {
	*x = BoardPriority{}
	if protoimpl.UnsafeEnabled {
		mi := &file_testplans_board_priorities_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BoardPriority) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BoardPriority) ProtoMessage() {}

func (x *BoardPriority) ProtoReflect() protoreflect.Message {
	mi := &file_testplans_board_priorities_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BoardPriority.ProtoReflect.Descriptor instead.
func (*BoardPriority) Descriptor() ([]byte, []int) {
	return file_testplans_board_priorities_proto_rawDescGZIP(), []int{0}
}

func (x *BoardPriority) GetSkylabBoard() string {
	if x != nil {
		return x.SkylabBoard
	}
	return ""
}

func (x *BoardPriority) GetPriority() int32 {
	if x != nil {
		return x.Priority
	}
	return 0
}

type BoardPriorityList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BoardPriorities []*BoardPriority `protobuf:"bytes,1,rep,name=board_priorities,json=boardPriorities,proto3" json:"board_priorities,omitempty"`
}

func (x *BoardPriorityList) Reset() {
	*x = BoardPriorityList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_testplans_board_priorities_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BoardPriorityList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BoardPriorityList) ProtoMessage() {}

func (x *BoardPriorityList) ProtoReflect() protoreflect.Message {
	mi := &file_testplans_board_priorities_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BoardPriorityList.ProtoReflect.Descriptor instead.
func (*BoardPriorityList) Descriptor() ([]byte, []int) {
	return file_testplans_board_priorities_proto_rawDescGZIP(), []int{1}
}

func (x *BoardPriorityList) GetBoardPriorities() []*BoardPriority {
	if x != nil {
		return x.BoardPriorities
	}
	return nil
}

var File_testplans_board_priorities_proto protoreflect.FileDescriptor

var file_testplans_board_priorities_proto_rawDesc = []byte{
	0x0a, 0x20, 0x74, 0x65, 0x73, 0x74, 0x70, 0x6c, 0x61, 0x6e, 0x73, 0x2f, 0x62, 0x6f, 0x61, 0x72,
	0x64, 0x5f, 0x70, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x69, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x09, 0x74, 0x65, 0x73, 0x74, 0x70, 0x6c, 0x61, 0x6e, 0x73, 0x22, 0x4e, 0x0a,
	0x0d, 0x42, 0x6f, 0x61, 0x72, 0x64, 0x50, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x12, 0x21,
	0x0a, 0x0c, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x5f, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x73, 0x6b, 0x79, 0x6c, 0x61, 0x62, 0x42, 0x6f, 0x61, 0x72,
	0x64, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x08, 0x70, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x22, 0x58, 0x0a,
	0x11, 0x42, 0x6f, 0x61, 0x72, 0x64, 0x50, 0x72, 0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x4c, 0x69,
	0x73, 0x74, 0x12, 0x43, 0x0a, 0x10, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x5f, 0x70, 0x72, 0x69, 0x6f,
	0x72, 0x69, 0x74, 0x69, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x18, 0x2e, 0x74,
	0x65, 0x73, 0x74, 0x70, 0x6c, 0x61, 0x6e, 0x73, 0x2e, 0x42, 0x6f, 0x61, 0x72, 0x64, 0x50, 0x72,
	0x69, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x52, 0x0f, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x50, 0x72, 0x69,
	0x6f, 0x72, 0x69, 0x74, 0x69, 0x65, 0x73, 0x42, 0x35, 0x5a, 0x33, 0x67, 0x6f, 0x2e, 0x63, 0x68,
	0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d,
	0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x70, 0x6c, 0x61, 0x6e, 0x73, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_testplans_board_priorities_proto_rawDescOnce sync.Once
	file_testplans_board_priorities_proto_rawDescData = file_testplans_board_priorities_proto_rawDesc
)

func file_testplans_board_priorities_proto_rawDescGZIP() []byte {
	file_testplans_board_priorities_proto_rawDescOnce.Do(func() {
		file_testplans_board_priorities_proto_rawDescData = protoimpl.X.CompressGZIP(file_testplans_board_priorities_proto_rawDescData)
	})
	return file_testplans_board_priorities_proto_rawDescData
}

var file_testplans_board_priorities_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_testplans_board_priorities_proto_goTypes = []interface{}{
	(*BoardPriority)(nil),     // 0: testplans.BoardPriority
	(*BoardPriorityList)(nil), // 1: testplans.BoardPriorityList
}
var file_testplans_board_priorities_proto_depIdxs = []int32{
	0, // 0: testplans.BoardPriorityList.board_priorities:type_name -> testplans.BoardPriority
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_testplans_board_priorities_proto_init() }
func file_testplans_board_priorities_proto_init() {
	if File_testplans_board_priorities_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_testplans_board_priorities_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BoardPriority); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_testplans_board_priorities_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BoardPriorityList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_testplans_board_priorities_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_testplans_board_priorities_proto_goTypes,
		DependencyIndexes: file_testplans_board_priorities_proto_depIdxs,
		MessageInfos:      file_testplans_board_priorities_proto_msgTypes,
	}.Build()
	File_testplans_board_priorities_proto = out.File
	file_testplans_board_priorities_proto_rawDesc = nil
	file_testplans_board_priorities_proto_goTypes = nil
	file_testplans_board_priorities_proto_depIdxs = nil
}
