// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.1.0
// - protoc             v3.17.1
// source: chromite/api/chrome_lkgm.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ChromeLkgmServiceClient is the client API for ChromeLkgmService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ChromeLkgmServiceClient interface {
	FindLkgm(ctx context.Context, in *FindLkgmRequest, opts ...grpc.CallOption) (*FindLkgmResponse, error)
}

type chromeLkgmServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewChromeLkgmServiceClient(cc grpc.ClientConnInterface) ChromeLkgmServiceClient {
	return &chromeLkgmServiceClient{cc}
}

func (c *chromeLkgmServiceClient) FindLkgm(ctx context.Context, in *FindLkgmRequest, opts ...grpc.CallOption) (*FindLkgmResponse, error) {
	out := new(FindLkgmResponse)
	err := c.cc.Invoke(ctx, "/chromite.api.ChromeLkgmService/FindLkgm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ChromeLkgmServiceServer is the server API for ChromeLkgmService service.
// All implementations must embed UnimplementedChromeLkgmServiceServer
// for forward compatibility
type ChromeLkgmServiceServer interface {
	FindLkgm(context.Context, *FindLkgmRequest) (*FindLkgmResponse, error)
	mustEmbedUnimplementedChromeLkgmServiceServer()
}

// UnimplementedChromeLkgmServiceServer must be embedded to have forward compatible implementations.
type UnimplementedChromeLkgmServiceServer struct {
}

func (UnimplementedChromeLkgmServiceServer) FindLkgm(context.Context, *FindLkgmRequest) (*FindLkgmResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindLkgm not implemented")
}
func (UnimplementedChromeLkgmServiceServer) mustEmbedUnimplementedChromeLkgmServiceServer() {}

// UnsafeChromeLkgmServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ChromeLkgmServiceServer will
// result in compilation errors.
type UnsafeChromeLkgmServiceServer interface {
	mustEmbedUnimplementedChromeLkgmServiceServer()
}

func RegisterChromeLkgmServiceServer(s grpc.ServiceRegistrar, srv ChromeLkgmServiceServer) {
	s.RegisterService(&ChromeLkgmService_ServiceDesc, srv)
}

func _ChromeLkgmService_FindLkgm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FindLkgmRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChromeLkgmServiceServer).FindLkgm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chromite.api.ChromeLkgmService/FindLkgm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChromeLkgmServiceServer).FindLkgm(ctx, req.(*FindLkgmRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ChromeLkgmService_ServiceDesc is the grpc.ServiceDesc for ChromeLkgmService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ChromeLkgmService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "chromite.api.ChromeLkgmService",
	HandlerType: (*ChromeLkgmServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "FindLkgm",
			Handler:    _ChromeLkgmService_FindLkgm_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "chromite/api/chrome_lkgm.proto",
}
