// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: chromite/api/ide.proto

package api

import (
	chromiumos "go.chromium.org/chromiumos/infra/proto/go/chromiumos"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GenerateMergedCompilationDBRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The chroot to use for generating the compdb.
	// The method expects that the build artifacts were generated and
	// stored in out_path.
	// Required.
	Chroot *chromiumos.Chroot `protobuf:"bytes,1,opt,name=chroot,proto3" json:"chroot,omitempty"`
	// The build target to generate a compdb for.
	// Required.
	BuildTarget *chromiumos.BuildTarget `protobuf:"bytes,2,opt,name=build_target,json=buildTarget,proto3" json:"build_target,omitempty"`
	// Directory to store merged build artifacts.
	// WARNING: existing dir if any will be completely removed.
	BuildDir *chromiumos.Path `protobuf:"bytes,3,opt,name=build_dir,json=buildDir,proto3" json:"build_dir,omitempty"`
	// Output file path for compile commands json.
	CompileCommands *chromiumos.Path `protobuf:"bytes,4,opt,name=compile_commands,json=compileCommands,proto3" json:"compile_commands,omitempty"`
	// List of packages to process.
	Packages []*chromiumos.PackageInfo `protobuf:"bytes,5,rep,name=packages,proto3" json:"packages,omitempty"`
	// Flag arguments.
	Flags *GenerateMergedCompilationDBRequest_Flags `protobuf:"bytes,6,opt,name=flags,proto3" json:"flags,omitempty"`
}

func (x *GenerateMergedCompilationDBRequest) Reset() {
	*x = GenerateMergedCompilationDBRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_chromite_api_ide_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GenerateMergedCompilationDBRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GenerateMergedCompilationDBRequest) ProtoMessage() {}

func (x *GenerateMergedCompilationDBRequest) ProtoReflect() protoreflect.Message {
	mi := &file_chromite_api_ide_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GenerateMergedCompilationDBRequest.ProtoReflect.Descriptor instead.
func (*GenerateMergedCompilationDBRequest) Descriptor() ([]byte, []int) {
	return file_chromite_api_ide_proto_rawDescGZIP(), []int{0}
}

func (x *GenerateMergedCompilationDBRequest) GetChroot() *chromiumos.Chroot {
	if x != nil {
		return x.Chroot
	}
	return nil
}

func (x *GenerateMergedCompilationDBRequest) GetBuildTarget() *chromiumos.BuildTarget {
	if x != nil {
		return x.BuildTarget
	}
	return nil
}

func (x *GenerateMergedCompilationDBRequest) GetBuildDir() *chromiumos.Path {
	if x != nil {
		return x.BuildDir
	}
	return nil
}

func (x *GenerateMergedCompilationDBRequest) GetCompileCommands() *chromiumos.Path {
	if x != nil {
		return x.CompileCommands
	}
	return nil
}

func (x *GenerateMergedCompilationDBRequest) GetPackages() []*chromiumos.PackageInfo {
	if x != nil {
		return x.Packages
	}
	return nil
}

func (x *GenerateMergedCompilationDBRequest) GetFlags() *GenerateMergedCompilationDBRequest_Flags {
	if x != nil {
		return x.Flags
	}
	return nil
}

type GenerateMergedCompilationDBResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *GenerateMergedCompilationDBResponse) Reset() {
	*x = GenerateMergedCompilationDBResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_chromite_api_ide_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GenerateMergedCompilationDBResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GenerateMergedCompilationDBResponse) ProtoMessage() {}

func (x *GenerateMergedCompilationDBResponse) ProtoReflect() protoreflect.Message {
	mi := &file_chromite_api_ide_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GenerateMergedCompilationDBResponse.ProtoReflect.Descriptor instead.
func (*GenerateMergedCompilationDBResponse) Descriptor() ([]byte, []int) {
	return file_chromite_api_ide_proto_rawDescGZIP(), []int{1}
}

type GenerateMergedCompilationDBRequest_Flags struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// If true, the execution stops on first failed package.
	FailFast bool `protobuf:"varint,1,opt,name=fail_fast,json=failFast,proto3" json:"fail_fast,omitempty"`
	// Whether to build tests alongside packages before generating.
	WithTests bool `protobuf:"varint,2,opt,name=with_tests,json=withTests,proto3" json:"with_tests,omitempty"`
}

func (x *GenerateMergedCompilationDBRequest_Flags) Reset() {
	*x = GenerateMergedCompilationDBRequest_Flags{}
	if protoimpl.UnsafeEnabled {
		mi := &file_chromite_api_ide_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GenerateMergedCompilationDBRequest_Flags) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GenerateMergedCompilationDBRequest_Flags) ProtoMessage() {}

func (x *GenerateMergedCompilationDBRequest_Flags) ProtoReflect() protoreflect.Message {
	mi := &file_chromite_api_ide_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GenerateMergedCompilationDBRequest_Flags.ProtoReflect.Descriptor instead.
func (*GenerateMergedCompilationDBRequest_Flags) Descriptor() ([]byte, []int) {
	return file_chromite_api_ide_proto_rawDescGZIP(), []int{0, 0}
}

func (x *GenerateMergedCompilationDBRequest_Flags) GetFailFast() bool {
	if x != nil {
		return x.FailFast
	}
	return false
}

func (x *GenerateMergedCompilationDBRequest_Flags) GetWithTests() bool {
	if x != nil {
		return x.WithTests
	}
	return false
}

var File_chromite_api_ide_proto protoreflect.FileDescriptor

var file_chromite_api_ide_proto_rawDesc = []byte{
	0x0a, 0x16, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x74, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x69,
	0x64, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69,
	0x74, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x1a, 0x1c, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x74, 0x65,
	0x2f, 0x61, 0x70, 0x69, 0x2f, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x5f, 0x61, 0x70, 0x69, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x17, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73,
	0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xc0, 0x03,
	0x0a, 0x22, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65, 0x4d, 0x65, 0x72, 0x67, 0x65, 0x64,
	0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x44, 0x42, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x2a, 0x0a, 0x06, 0x63, 0x68, 0x72, 0x6f, 0x6f, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f,
	0x73, 0x2e, 0x43, 0x68, 0x72, 0x6f, 0x6f, 0x74, 0x52, 0x06, 0x63, 0x68, 0x72, 0x6f, 0x6f, 0x74,
	0x12, 0x3a, 0x0a, 0x0c, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x5f, 0x74, 0x61, 0x72, 0x67, 0x65, 0x74,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75,
	0x6d, 0x6f, 0x73, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x54, 0x61, 0x72, 0x67, 0x65, 0x74, 0x52,
	0x0b, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x54, 0x61, 0x72, 0x67, 0x65, 0x74, 0x12, 0x2d, 0x0a, 0x09,
	0x62, 0x75, 0x69, 0x6c, 0x64, 0x5f, 0x64, 0x69, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x10, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2e, 0x50, 0x61, 0x74,
	0x68, 0x52, 0x08, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x44, 0x69, 0x72, 0x12, 0x3b, 0x0a, 0x10, 0x63,
	0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x65, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x61, 0x6e, 0x64, 0x73, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x10, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d,
	0x6f, 0x73, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x52, 0x0f, 0x63, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x65,
	0x43, 0x6f, 0x6d, 0x6d, 0x61, 0x6e, 0x64, 0x73, 0x12, 0x33, 0x0a, 0x08, 0x70, 0x61, 0x63, 0x6b,
	0x61, 0x67, 0x65, 0x73, 0x18, 0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x63, 0x68, 0x72,
	0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2e, 0x50, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x49,
	0x6e, 0x66, 0x6f, 0x52, 0x08, 0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x73, 0x12, 0x4c, 0x0a,
	0x05, 0x66, 0x6c, 0x61, 0x67, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x36, 0x2e, 0x63,
	0x68, 0x72, 0x6f, 0x6d, 0x69, 0x74, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x6e, 0x65,
	0x72, 0x61, 0x74, 0x65, 0x4d, 0x65, 0x72, 0x67, 0x65, 0x64, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x44, 0x42, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x46,
	0x6c, 0x61, 0x67, 0x73, 0x52, 0x05, 0x66, 0x6c, 0x61, 0x67, 0x73, 0x1a, 0x43, 0x0a, 0x05, 0x46,
	0x6c, 0x61, 0x67, 0x73, 0x12, 0x1b, 0x0a, 0x09, 0x66, 0x61, 0x69, 0x6c, 0x5f, 0x66, 0x61, 0x73,
	0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x08, 0x66, 0x61, 0x69, 0x6c, 0x46, 0x61, 0x73,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x77, 0x69, 0x74, 0x68, 0x5f, 0x74, 0x65, 0x73, 0x74, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x09, 0x77, 0x69, 0x74, 0x68, 0x54, 0x65, 0x73, 0x74, 0x73,
	0x22, 0x25, 0x0a, 0x23, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65, 0x4d, 0x65, 0x72, 0x67,
	0x65, 0x64, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x44, 0x42, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x32, 0xa4, 0x01, 0x0a, 0x0a, 0x49, 0x64, 0x65, 0x53,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x8a, 0x01, 0x0a, 0x1b, 0x47, 0x65, 0x6e, 0x65, 0x72,
	0x61, 0x74, 0x65, 0x4d, 0x65, 0x72, 0x67, 0x65, 0x64, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x44, 0x42, 0x12, 0x30, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x74,
	0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65, 0x4d, 0x65,
	0x72, 0x67, 0x65, 0x64, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x44,
	0x42, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x31, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d,
	0x69, 0x74, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65,
	0x4d, 0x65, 0x72, 0x67, 0x65, 0x64, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x44, 0x42, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x06, 0xc2, 0xed, 0x1a,
	0x02, 0x10, 0x02, 0x1a, 0x09, 0xc2, 0xed, 0x1a, 0x05, 0x0a, 0x03, 0x69, 0x64, 0x65, 0x42, 0x38,
	0x5a, 0x36, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72,
	0x67, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x6f, 0x73, 0x2f, 0x69, 0x6e, 0x66,
	0x72, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x6f, 0x2f, 0x63, 0x68, 0x72, 0x6f,
	0x6d, 0x69, 0x74, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_chromite_api_ide_proto_rawDescOnce sync.Once
	file_chromite_api_ide_proto_rawDescData = file_chromite_api_ide_proto_rawDesc
)

func file_chromite_api_ide_proto_rawDescGZIP() []byte {
	file_chromite_api_ide_proto_rawDescOnce.Do(func() {
		file_chromite_api_ide_proto_rawDescData = protoimpl.X.CompressGZIP(file_chromite_api_ide_proto_rawDescData)
	})
	return file_chromite_api_ide_proto_rawDescData
}

var file_chromite_api_ide_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_chromite_api_ide_proto_goTypes = []interface{}{
	(*GenerateMergedCompilationDBRequest)(nil),       // 0: chromite.api.GenerateMergedCompilationDBRequest
	(*GenerateMergedCompilationDBResponse)(nil),      // 1: chromite.api.GenerateMergedCompilationDBResponse
	(*GenerateMergedCompilationDBRequest_Flags)(nil), // 2: chromite.api.GenerateMergedCompilationDBRequest.Flags
	(*chromiumos.Chroot)(nil),                        // 3: chromiumos.Chroot
	(*chromiumos.BuildTarget)(nil),                   // 4: chromiumos.BuildTarget
	(*chromiumos.Path)(nil),                          // 5: chromiumos.Path
	(*chromiumos.PackageInfo)(nil),                   // 6: chromiumos.PackageInfo
}
var file_chromite_api_ide_proto_depIdxs = []int32{
	3, // 0: chromite.api.GenerateMergedCompilationDBRequest.chroot:type_name -> chromiumos.Chroot
	4, // 1: chromite.api.GenerateMergedCompilationDBRequest.build_target:type_name -> chromiumos.BuildTarget
	5, // 2: chromite.api.GenerateMergedCompilationDBRequest.build_dir:type_name -> chromiumos.Path
	5, // 3: chromite.api.GenerateMergedCompilationDBRequest.compile_commands:type_name -> chromiumos.Path
	6, // 4: chromite.api.GenerateMergedCompilationDBRequest.packages:type_name -> chromiumos.PackageInfo
	2, // 5: chromite.api.GenerateMergedCompilationDBRequest.flags:type_name -> chromite.api.GenerateMergedCompilationDBRequest.Flags
	0, // 6: chromite.api.IdeService.GenerateMergedCompilationDB:input_type -> chromite.api.GenerateMergedCompilationDBRequest
	1, // 7: chromite.api.IdeService.GenerateMergedCompilationDB:output_type -> chromite.api.GenerateMergedCompilationDBResponse
	7, // [7:8] is the sub-list for method output_type
	6, // [6:7] is the sub-list for method input_type
	6, // [6:6] is the sub-list for extension type_name
	6, // [6:6] is the sub-list for extension extendee
	0, // [0:6] is the sub-list for field type_name
}

func init() { file_chromite_api_ide_proto_init() }
func file_chromite_api_ide_proto_init() {
	if File_chromite_api_ide_proto != nil {
		return
	}
	file_chromite_api_build_api_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_chromite_api_ide_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GenerateMergedCompilationDBRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_chromite_api_ide_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GenerateMergedCompilationDBResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_chromite_api_ide_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GenerateMergedCompilationDBRequest_Flags); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_chromite_api_ide_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_chromite_api_ide_proto_goTypes,
		DependencyIndexes: file_chromite_api_ide_proto_depIdxs,
		MessageInfos:      file_chromite_api_ide_proto_msgTypes,
	}.Build()
	File_chromite_api_ide_proto = out.File
	file_chromite_api_ide_proto_rawDesc = nil
	file_chromite_api_ide_proto_goTypes = nil
	file_chromite_api_ide_proto_depIdxs = nil
}
