// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.1.0
// - protoc             v3.17.1
// source: chromite/api/relevancy.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// RelevancyServiceClient is the client API for RelevancyService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RelevancyServiceClient interface {
	// Get the relevant build targets for a change.
	GetRelevantBuildTargets(ctx context.Context, in *GetRelevantBuildTargetsRequest, opts ...grpc.CallOption) (*GetRelevantBuildTargetsResponse, error)
}

type relevancyServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRelevancyServiceClient(cc grpc.ClientConnInterface) RelevancyServiceClient {
	return &relevancyServiceClient{cc}
}

func (c *relevancyServiceClient) GetRelevantBuildTargets(ctx context.Context, in *GetRelevantBuildTargetsRequest, opts ...grpc.CallOption) (*GetRelevantBuildTargetsResponse, error) {
	out := new(GetRelevantBuildTargetsResponse)
	err := c.cc.Invoke(ctx, "/chromite.api.RelevancyService/GetRelevantBuildTargets", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RelevancyServiceServer is the server API for RelevancyService service.
// All implementations must embed UnimplementedRelevancyServiceServer
// for forward compatibility
type RelevancyServiceServer interface {
	// Get the relevant build targets for a change.
	GetRelevantBuildTargets(context.Context, *GetRelevantBuildTargetsRequest) (*GetRelevantBuildTargetsResponse, error)
	mustEmbedUnimplementedRelevancyServiceServer()
}

// UnimplementedRelevancyServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRelevancyServiceServer struct {
}

func (UnimplementedRelevancyServiceServer) GetRelevantBuildTargets(context.Context, *GetRelevantBuildTargetsRequest) (*GetRelevantBuildTargetsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetRelevantBuildTargets not implemented")
}
func (UnimplementedRelevancyServiceServer) mustEmbedUnimplementedRelevancyServiceServer() {}

// UnsafeRelevancyServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RelevancyServiceServer will
// result in compilation errors.
type UnsafeRelevancyServiceServer interface {
	mustEmbedUnimplementedRelevancyServiceServer()
}

func RegisterRelevancyServiceServer(s grpc.ServiceRegistrar, srv RelevancyServiceServer) {
	s.RegisterService(&RelevancyService_ServiceDesc, srv)
}

func _RelevancyService_GetRelevantBuildTargets_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetRelevantBuildTargetsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RelevancyServiceServer).GetRelevantBuildTargets(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chromite.api.RelevancyService/GetRelevantBuildTargets",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RelevancyServiceServer).GetRelevantBuildTargets(ctx, req.(*GetRelevantBuildTargetsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// RelevancyService_ServiceDesc is the grpc.ServiceDesc for RelevancyService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RelevancyService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "chromite.api.RelevancyService",
	HandlerType: (*RelevancyServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetRelevantBuildTargets",
			Handler:    _RelevancyService_GetRelevantBuildTargets_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "chromite/api/relevancy.proto",
}
