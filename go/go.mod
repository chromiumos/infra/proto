module go.chromium.org/chromiumos/infra/proto/go

go 1.12

require (
	github.com/golang/protobuf v1.4.3
	go.chromium.org/chromiumos/config/go v0.0.0-20210225201405-02ec5b5e84b7
	golang.org/x/net v0.0.0-20210224082022-3d97a244fca7 // indirect
	golang.org/x/sys v0.0.0-20210225134936-a50acf3fe073 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210225212918-ad91960f0274
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
