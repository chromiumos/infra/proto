# proto/src/ide_query

This directory contains the protobuf source files used by Cider for

## ide_query/

This directory contains the response proto for the ide_query script, defined at go/reqs-for-peep. It should be pulled down from google3/devtools/cider/services/build/companion/ide_query.proto.
