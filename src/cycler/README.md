Here lives the protocol buffers definitions for cycler.

Internal Design Doc: go/cros-gs-lifecycler

config.proto:       Individual configurations (logging, running, stats).
action.proto:       Action configurations.
