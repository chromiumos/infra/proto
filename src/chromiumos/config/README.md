** Do not add packages under this directory **

This directory is a placeholder for protobufs imported under the name space

`chromiumos.config.*`

from
https://chromium.googlesource.com/chromiumos/config/+/HEAD/proto/chromiumos/config

Adding protobuf definitions locally under this directory is strongly discouraged
as it will lead to namespace collision.
